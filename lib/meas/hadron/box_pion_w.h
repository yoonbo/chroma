// -*- C++ -*-
/*! \file
 *  \brief Box digrams for pions
 */

#ifndef __box_pion_h__
#define __box_pion_h__

#include "io/xml_group_reader.h"
#include "actions/ferm/invert/syssolver_linop_aggregate.h"
#include "actions/ferm/fermacts/fermact_factory_w.h"
#include "actions/ferm/fermacts/fermacts_aggregate_w.h"
#include "meas/inline/io/named_objmap.h"

namespace Chroma {

//! Box diagrams for pions
/* 
 * Calculate box diagrams for pions
 *
 * \param list_props  propagator pairs ( Read )
 * \param list_tsrcs  source timeslices of the propagators ( Read )
 * \param T_ins       timeslice of current insertion ( Read )
 * \param N_ins       number of of current insertions ( Read )
 * \param rseed       random seed for random current insertions ( Read )
 * \param IDiagAB     If calculate diag AB ( Read )
 * \param IDiagC     If calculate diag C ( Read )
 * \param xml         xml file object ( Write )
 *
 */

void box_pion(multi2d<const LatticePropagator*> &list_props,
	    multi2d<int> &list_tsrcs,
      multi1d<std::string> &out_fnames,
      const int T_ins,
      const int N_ins,
      const int rseed,
      const bool IDiagAB,
      const bool IDiagC,
      const GroupXML_t  &invParam,
      const GroupXML_t  &invParam_1,
      const GroupXML_t  &fermact,
      const bool        &setupParam_1,
      const multi1d<LatticeColorMatrix> &u,
	    XMLWriter& xml
      ) ;
  
}  // end namespace Chroma

#endif
