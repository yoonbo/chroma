// $Id: curcor2_w.cc,v 3.0 2006-04-03 04:58:59 edwards Exp $
/*! \file
 *  \brief Mesonic current correlators
 */

#include "chromabase.h"
#include "util/ft/sftmom.h"
#include "meas/hadron/mesons_w.h"
#include "qdp_util.h"                 // part of QDP++, for crtesn()
#include "curcor2_w_cedm.h"

namespace Chroma {

//! Construct current correlators 
/*!
 * \ingroup hadron
 *
 * This routine is specific to Wilson fermions!
 *
 *  The two propagators can be identical or different.

 * This includes the "rho_1--rho_2" correlators used for O(a) improvement

 * For use with "rotated" propagators we added the possibility of also
 * computing the local std::vector current, when no_vec_cur = 4. In this
 * case the 3 local currents come last.

 * \param u               gauge field ( Read )
 * \param quark_prop_1    first quark propagator ( Read )
 * \param quark_prop_2    second (anti-) quark propagator ( Read )
 * \param phases          fourier transform phase factors ( Read )
 * \param t0              timeslice coordinate of the source ( Read )
 * \param no_vec_cur      number of std::vector current types, 3 or 4 ( Read )
 * \param ExtVCurrentP    Extended vector currents ( Read )
 * \param ExtACurrentP    Extended axial currents ( Read )
 * \param ExtCEDMP        Extended currents for CEDM calculation ( Read )
 * \param xml             namelist file object ( Read )
 * \param xml_group       std::string used for writing xml data ( Read )
 *
 *         ____
 *         \
 * cc(t) =  >  < m(t_source, 0) c(t + t_source, x) >
 *         /                    
 *         ----
 *           x
 */


void curcor2(const multi1d<LatticeColorMatrix>& u, 
	     const LatticePropagator& quark_prop_1, 
	     const LatticePropagator& quark_prop_2, 
	     const SftMom& phases,
	     int t0,
	     int no_vec_cur,
       bool ExtVCurrentP,
       bool ExtACurrentP,
       bool ExtCEDMP,
	     XMLWriter& xml,
	     const std::string& xml_group)
{
  START_CODE();

  if ( no_vec_cur < 2 || no_vec_cur > 4 )
    QDP_error_exit("no_vec_cur must be 2 or 3 or 4", no_vec_cur);

  // Initial group
  push(xml, xml_group);

  write(xml, "num_vec_cur", no_vec_cur);

  // Length of lattice in decay direction
  int length  = phases.numSubsets();
  int j_decay = phases.getDir();

  LatticePropagator tmp_prop1;
  LatticePropagator tmp_prop2;

  LatticeReal psi_sq;
  LatticeReal chi_sq;

  multi1d<Double> hsum(length);

  // Construct the anti-quark propagator from quark_prop_2
  int G5 = Ns*Ns-1;
  LatticePropagator anti_quark_prop =  Gamma(G5) * quark_prop_2 * Gamma(G5);

  // Vector currents
  {
    multi2d<Real> vector_current(no_vec_cur*(Nd-1), length);

    /* Construct the 2*(Nd-1) non-local std::vector-current to rho correlators */
    int kv = -1;
    int kcv = Nd-2;

    for(int k = 0; k < Nd; ++k)
    {
      if( k != j_decay )
      {
	int n = 1 << k;
	kv = kv + 1;
	kcv = kcv + 1;

	tmp_prop2 = u[k] * shift(quark_prop_1, FORWARD, k) * Gamma(n);
	chi_sq = - real(trace(adj(anti_quark_prop) * tmp_prop2));

	tmp_prop1 = Gamma(n) * tmp_prop2;
	psi_sq = real(trace(adj(anti_quark_prop) * tmp_prop1));

	tmp_prop2 = u[k] * shift(anti_quark_prop, FORWARD, k) * Gamma(n);
	chi_sq += real(trace(adj(tmp_prop2) * quark_prop_1));

	tmp_prop1 = Gamma(n) * tmp_prop2;
	psi_sq += real(trace(adj(tmp_prop1) * quark_prop_1));

	chi_sq += psi_sq;

	/* Do a slice-wise sum. */

//    Real dummy1 = Real(meson_eta[n]) / Real(2);
	Real dummy1 = 0.5;

	/* The nonconserved std::vector current first */
	hsum = sumMulti(psi_sq, phases.getSet());

	for(int t = 0; t < length; ++t)
	{
	  int t_eff = (t - t0 + length) % length;
	  vector_current[kv][t_eff] = dummy1 * Real(hsum[t]);
	}

	/* The conserved std::vector current next */
	hsum = sumMulti(chi_sq, phases.getSet());

	for(int t = 0; t < length; ++t)
	{
	  int t_eff = (t - t0 + length) % length;
	  vector_current[kcv][t_eff] = dummy1 * Real(hsum[t]);
	}
      }
    }


    /* Construct the O(a) improved std::vector-current to rho correlators,
       if desired */
    if ( no_vec_cur >= 3 )
    {
      kv = 2*Nd-3;
      int jd = 1 << j_decay;

      for(int k = 0; k < Nd; ++k)
      {
	if( k != j_decay )
	{
	  int n = 1 << k;
	  kv = kv + 1;
	  int n1 = n ^ jd;

	  psi_sq = real(trace(adj(anti_quark_prop) * Gamma(n1) * quark_prop_1 * Gamma(n)));

//	dummy1 = - Real(meson_eta[n]);
	  Real dummy1 = - 1;

	  /* Do a slice-wise sum. */
	  hsum = sumMulti(psi_sq, phases.getSet());

	  for(int t = 0; t < length; ++t)
	  {
	    int t_eff = (t - t0 + length) % length;
	    vector_current[kv][t_eff] = dummy1 * Real(hsum[t]);
	  }
	}
      }
    }


    /* Construct the local std::vector-current to rho correlators, if desired */
    if ( no_vec_cur >= 4 )
    {
      kv = 3*Nd-4;

      for(int k = 0; k < Nd; ++k)
      {
	if( k != j_decay )
	{
	  int n = 1 << k;
	  kv = kv + 1;

	  psi_sq = real(trace(adj(anti_quark_prop) * Gamma(n) * quark_prop_1 * Gamma(n)));

//	dummy1 = Real(meson_eta[n]);
	  Real dummy1 = 1;

	  /* Do a slice-wise sum. */
	  hsum = sumMulti(psi_sq, phases.getSet());

	  for(int t = 0; t < length; ++t)
	  {
	    int t_eff = (t - t0 + length) % length;
	    vector_current[kv][t_eff] = dummy1 * Real(hsum[t]);
	  }
	}
      }
    }


    // Loop over currents to print
    XMLArrayWriter xml_cur(xml,vector_current.size2());
    push(xml_cur, "Vector_currents");

    for (int current_value=0; current_value < vector_current.size2(); ++current_value)
    {
      push(xml_cur);     // next array element

      write(xml_cur, "current_value", current_value);
      write(xml_cur, "vector_current", vector_current[current_value]);

      pop(xml_cur);
    }

    pop(xml_cur);
  }


  //
  // Axial currents
  //
  {
    multi2d<Real> axial_current(2, length);

    /* Construct the 2 axial-current to pion correlators */
    int n = G5 ^ (1 << j_decay);

    /* The local axial current first */
    psi_sq = real(trace(adj(anti_quark_prop) * Gamma(n) * quark_prop_1 * Gamma(G5)));

    /* The nonlocal axial current next */
    chi_sq  = real(trace(adj(anti_quark_prop) * Gamma(n) * 
			 u[j_decay] * shift(quark_prop_1, FORWARD, j_decay) * Gamma(G5)));

    // The () forces precedence
    chi_sq -= real(trace(adj(Gamma(n) * (u[j_decay] * shift(anti_quark_prop, FORWARD, j_decay)) * 
			     Gamma(G5)) * quark_prop_1));

    /* Do a slice-wise sum. */

    Real dummy1 = Real(-1) / Real(2);

    /* The local axial current first */
    hsum = sumMulti(psi_sq, phases.getSet());

    for(int t = 0; t < length; ++t)
    {
      int t_eff = (t - t0 + length) % length;
      axial_current[1][t_eff] = - Real(hsum[t]);
    }

    /* The nonlocal axial current next */
    hsum = sumMulti(chi_sq, phases.getSet());

    for(int t = 0; t < length; ++t)
    {
      int t_eff = (t - t0 + length) % length;
      axial_current[0][t_eff] = dummy1 * Real(hsum[t]);
    }

    // Loop over currents to print
    XMLArrayWriter xml_cur(xml,axial_current.size2());
    push(xml_cur, "Axial_currents");

    for (int current_value=0; current_value < axial_current.size2(); ++current_value)
    {
      push(xml_cur);     // next array element

      write(xml_cur, "current_value", current_value);
      write(xml_cur, "axial_current", axial_current[current_value]);

      pop(xml_cur);
    }

    pop(xml_cur);
  }





  //--------------------------
  // Block: Extended Currents
  //--------------------------
  if(ExtVCurrentP || ExtVCurrentP)
  {
    if(j_decay != 3)
    {
      QDPIO::cerr << "Error!! Extended currents work only when j_decay == 3" << std::endl;
      QDP_abort(1);
    }

    LatticeComplex corr_fn;
    multi2d<DComplex> ext_hsum;

    //-----------------------------
    // Extended vector current
    //-----------------------------

    if(ExtVCurrentP)
    {
      enum N_pairs {N_pairs = 58};
      int idx_pairs[N_pairs][2] = {
        { 1,  1}, // V1-V1
        { 2,  2}, // V2-V2
        { 4,  4}, // V3-V3
        { 8,  8}, // V4-V4
        { 1,  3}, // V1-T12
        { 1,  5}, // V1-T13
        { 1,  9}, // V1-T14
        { 2,  3}, // V2-T12
        { 2,  6}, // V2-T23
        { 2, 10}, // V2-T24
        { 4,  5}, // V3-T13
        { 4,  6}, // V3-T23
        { 4, 12}, // V3-T34
        { 8,  9}, // V4-T14
        { 8, 10}, // V4-T24
        { 8, 12}, // V4-T34 
        { 3,  1}, // T12-V1
        { 5,  1}, // T13-V1
        { 9,  1}, // T14-V1
        { 3,  2}, // T12-V2
        { 6,  2}, // T23-V2
        {10,  2}, // T24-V2
        { 5,  4}, // T13-V3
        { 6,  4}, // T23-V3
        {12,  4}, // T34-V3
        { 9,  8}, // T14-V4
        {10,  8}, // T24-V4
        {12,  8}, // T34-V4
        { 3,  3}, // T12-T12
        { 3,  5}, // T12-T13
        { 3,  9}, // T12-T14
        { 3,  6}, // T12-T23
        { 3, 10}, // T12-T24
        { 5,  3}, // T13-T12
        { 5,  5}, // T13-T13
        { 5,  9}, // T13-T14
        { 5,  6}, // T13-T23
        { 5, 12}, // T13-T34
        { 9,  3}, // T14-T12
        { 9,  5}, // T14-T13
        { 9,  9}, // T14-T14
        { 9, 10}, // T14-T24
        { 9, 12}, // T14-T34
        { 6,  3}, // T23-T12
        { 6,  5}, // T23-T13
        { 6,  6}, // T23-T23
        { 6, 10}, // T23-T24
        { 6, 12}, // T23-T34
        {10,  3}, // T24-T12
        {10,  9}, // T24-T14
        {10,  6}, // T24-T23
        {10, 10}, // T24-T24
        {10, 12}, // T24-T34
        {12,  5}, // T34-T13
        {12,  9}, // T34-T14
        {12,  6}, // T34-T23
        {12, 10}, // T34-T24
        {12, 12}  // T34-T34
      };

      
      // Loop over currents to print
      XMLArrayWriter xml_cur(xml,N_pairs);
      push(xml_cur, "Extended_vector_currents");
      for(int idx=0; idx<N_pairs; ++idx)
      {
        push(xml_cur);     // next array element
        write(xml_cur, "current_value", idx);

        int sgn_i = std::copysign(1, idx_pairs[idx][0]);
        int sgn_j = std::copysign(1, idx_pairs[idx][1]);
  
        int i = std::abs(idx_pairs[idx][0]);
        int j = std::abs(idx_pairs[idx][1]);
  
        corr_fn  = sgn_i * sgn_j * trace(adj(anti_quark_prop) * Gamma(i) * quark_prop_1 * Gamma(j));
        ext_hsum = phases.sft(corr_fn);


        // Loop over sink momenta
        XMLArrayWriter xml_sink_mom(xml_cur,phases.numMom());
        push(xml_sink_mom, "momenta");

        for (int sink_mom_num=0; sink_mom_num < phases.numMom(); ++sink_mom_num)
        {
          push(xml_sink_mom);
          write(xml_sink_mom, "sink_mom_num", sink_mom_num);
          write(xml_sink_mom, "sink_mom", phases.numToMom(sink_mom_num));

          multi1d<DComplex> extcur(length);
          for (int t=0; t < length; ++t)
          {
            int t_eff = (t - t0 + length) % length;
            extcur[t_eff] = ext_hsum[sink_mom_num][t];
          }

          write(xml_sink_mom, "extcur", extcur);
          pop(xml_sink_mom);
        } // end for(sink_mom_num)
        pop(xml_sink_mom);
        pop(xml_cur);
      } // for(int idx=0; idx<N_pairs; ++idx)
      pop(xml_cur);
    } // if(ExtVCurrentP)


    //-----------------------------
    // Extended axial current
    //-----------------------------

    if(ExtACurrentP)
    {
      enum N_pairs {N_pairs = 13};
      int idx_pairs[N_pairs][2] = {
        // Ai = \gamma_i gamma_5
        { 15,  15}, // P-P
        { 15,  14}, // P-A1
        { 15, -13}, // P-A2
        { 15,  11}, // P-A3
        { 15, -7 }, // P-A4
        { 14,  15}, // A1-P
        { 14,  14}, // A1-A1
        {-13,  15}, // A2-P
        {-13, -13}, // A2-A2
        { 11,  15}, // A3-P
        { 11,  11}, // A3-A3
        {-7 ,  15}, // A4-P
        {-7 , -7 }  // A4-A4
      };
      
      
      // Loop over currents to print
      XMLArrayWriter xml_cur(xml,N_pairs);
      push(xml_cur, "Extended_axial_currents");
      for(int idx=0; idx<N_pairs; ++idx)
      {
        push(xml_cur);     // next array element
        write(xml_cur, "current_value", idx);

        int sgn_i = std::copysign(1, idx_pairs[idx][0]);
        int sgn_j = std::copysign(1, idx_pairs[idx][1]);
  
        int i = std::abs(idx_pairs[idx][0]);
        int j = std::abs(idx_pairs[idx][1]);
  
        corr_fn  = sgn_i * sgn_j * trace(adj(anti_quark_prop) * Gamma(i) * quark_prop_1 * Gamma(j));
        ext_hsum = phases.sft(corr_fn);


        // Loop over sink momenta
        XMLArrayWriter xml_sink_mom(xml_cur,phases.numMom());
        push(xml_sink_mom, "momenta");

        for (int sink_mom_num=0; sink_mom_num < phases.numMom(); ++sink_mom_num)
        {
          push(xml_sink_mom);
          write(xml_sink_mom, "sink_mom_num", sink_mom_num);
          write(xml_sink_mom, "sink_mom", phases.numToMom(sink_mom_num));

          multi1d<DComplex> extcur(length);
          for (int t=0; t < length; ++t)
          {
            int t_eff = (t - t0 + length) % length;
            extcur[t_eff] = ext_hsum[sink_mom_num][t];
          }

          write(xml_sink_mom, "extcur", extcur);
          pop(xml_sink_mom);
        } // end for(sink_mom_num)
        pop(xml_sink_mom);
        pop(xml_cur);
      } // for(int idx=0; idx<N_pairs; ++idx)
      pop(xml_cur);
    } // if(ExtACurrentP)

  } // End of block Extended Currents: if(ExtVCurrentP || ExtVCurrentP)


  //-----------------------------
  // Extended currents for CEDM calculation
  //-----------------------------

  if(ExtCEDMP)
  {
    if(j_decay != 3)
    {
      QDPIO::cerr << "Error!! Extended currents work only when j_decay == 3" << std::endl;
      QDP_abort(1);
    }

    LatticeComplex corr_fn;
    multi2d<DComplex> ext_hsum;

    enum N_pairs {N_pairs = 11};
    int idx_pairs[N_pairs][2] = {
      // Ai = \gamma_i gamma_5
      { 15,  15}, // P-P
      { 15,  14}, // P-A1
      { 15, -13}, // P-A2
      { 15,  11}, // P-A3
      { 15, -7 }, // P-A4
      { 14,  15}, // A1-P
      {-13,  15}, // A2-P
      { 11,  15}, // A3-P
      {-7 ,  15}, // A4-P
      { 15,  99}, // P-CEDM
      { 99,  15}  // CEDM-P
    };
    
    // WARNING:
    // Note that the pseudoscalar operator for CEDM is
    //   i * psi * g5 * psi
    // but what we calcualte here is 
    //   psi * g5 * psi
    // To obtain the imaginary part of (i * psi * g5 * psi), take real part of what we calculate

    // Calculate CEDM term
    LatticePropagator CEDM;
    calc_cEDM(u, CEDM);


    // Loop over currents to print
    XMLArrayWriter xml_cur(xml,N_pairs);
    push(xml_cur, "Extended_CEDM_currents");
    for(int idx=0; idx<N_pairs; ++idx)
    {
      push(xml_cur);     // next array element
      write(xml_cur, "current_value", idx);

      int sgn_i = std::copysign(1, idx_pairs[idx][0]);
      int sgn_j = std::copysign(1, idx_pairs[idx][1]);

      int i = std::abs(idx_pairs[idx][0]);
      int j = std::abs(idx_pairs[idx][1]);

      if(i == 99)
        corr_fn  = sgn_i * sgn_j * trace(adj(anti_quark_prop) * CEDM * quark_prop_1 * Gamma(j));
      else if(j == 99)
        corr_fn  = sgn_i * sgn_j * trace(adj(anti_quark_prop) * Gamma(i) * quark_prop_1 * CEDM);
      else
        corr_fn  = sgn_i * sgn_j * trace(adj(anti_quark_prop) * Gamma(i) * quark_prop_1 * Gamma(j));

      ext_hsum = phases.sft(corr_fn);


      // Loop over sink momenta
      XMLArrayWriter xml_sink_mom(xml_cur,phases.numMom());
      push(xml_sink_mom, "momenta");

      for (int sink_mom_num=0; sink_mom_num < phases.numMom(); ++sink_mom_num)
      {
        push(xml_sink_mom);
        write(xml_sink_mom, "sink_mom_num", sink_mom_num);
        write(xml_sink_mom, "sink_mom", phases.numToMom(sink_mom_num));

        multi1d<DComplex> extcur(length);
        for (int t=0; t < length; ++t)
        {
          int t_eff = (t - t0 + length) % length;
          extcur[t_eff] = ext_hsum[sink_mom_num][t];
        }

        write(xml_sink_mom, "extcur", extcur);
        pop(xml_sink_mom);
      } // end for(sink_mom_num)
      pop(xml_sink_mom);
      pop(xml_cur);
    } // for(int idx=0; idx<N_pairs; ++idx)
    pop(xml_cur);
  } // End of block Extended currents for CEDM calculation


  pop(xml);  // xml_group
              
  END_CODE();
}

}  // end namespace Chroma
