/*! \file
 *  \brief Nucleon 2-pt functions with projection
 */


#include "meas/hadron/nucl_2pt_proj_w.h"
#include "meas/hadron/barspinmat_w.h"

namespace Chroma 
{
  //! Nucleon 2-pt functions
  /*!
   * \ingroup hadron
   *
   * This routine is specific to Wilson fermions! 
   *
   * Construct baryon propagators for the Proton with
   * degenerate "u" and "d" quarks.

   * \param propagator_1   "s" quark propagator ( Read )
   * \param propagator_2   "u" quark propagator ( Read )
   * \param t0             cartesian coordinates of the source ( Read )
   * \param bc_spec        boundary condition for spectroscopy ( Read )
   * \param phases         object holds list of momenta and Fourier phases ( Read )
   * \param xml            xml file object ( Read )
   * \param xml_group      group name for xml data ( Read )
   * \param cEDM           if measurement for chromo EDM
   *
   */

  void nucl_2pt_proj(const LatticePropagator& propagator_1, 
      const LatticePropagator& propagator_2, 
      const SftMom& phases,
      int t0, multi1d<SftMomSrcPos_t> &multi_srcs, int bc_spec,
      bool axial_proj_only,
      XMLWriter& xml,
      const std::string& xml_group,
      bool cEDM)
  {
    START_CODE();

    if ( Ns != 4 || Nc != 3 )		/* Code is specific to Ns=4 and Nc=3. */
      return;

    multi3d<DComplex> bardisp1;
    multi3d<DComplex> bardisp2;

    // Forward
    nucl_2pt_proj(propagator_1, propagator_2, phases, bardisp1, axial_proj_only, cEDM);

    int num_baryons = bardisp1.size3();
    int num_mom = bardisp1.size2();
    int length  = bardisp1.size1();

    // Loop over baryons
    XMLArrayWriter xml_bar(xml,num_baryons);
    push(xml_bar, xml_group);

    for(int baryons = 0; baryons < num_baryons; ++baryons)
    {
      push(xml_bar);     // next array element
      write(xml_bar, "baryon_num", baryons);

      // Loop over sink momenta
      XMLArrayWriter xml_sink_mom(xml_bar,num_mom);
      push(xml_sink_mom, "momenta");

      for(int sink_mom_num = 0; sink_mom_num < num_mom; ++sink_mom_num)
      {
        push(xml_sink_mom);
        write(xml_sink_mom, "sink_mom_num", sink_mom_num) ;
        write(xml_sink_mom, "sink_mom", phases.numToMom(sink_mom_num)) ;

        multi1d<Complex> barprop(length);
        for(int i=0; i<length; ++i)
          barprop[i] = zero;

        // Multi sources placed
        if(multi_srcs.size() > 0)
        {
          // Find out if t0 is in 
          // tA (forward propagation) or tB (backward propagation)
          int prop = 0; // 1 : fwd,  2 : bwd
          for(int idx_src=0; idx_src<multi_srcs.size(); ++idx_src)
          {
            int tA = multi_srcs[idx_src].tA;
            int tB = multi_srcs[idx_src].tB;

            if(t0 == tA)
            {
              prop = 1;
              break;
            }
            else if (t0 == tB)
            {
              prop = -1;
              break;
            }
          } // for(idx_src)

          if(prop == 0)
            QDP_error_exit("tA or tB do not match with t0.");

          // Multiply sign if necessary
          for(int idx_src=0; idx_src<multi_srcs.size(); ++idx_src)
          {
            int tA = multi_srcs[idx_src].tA;
            int tB = multi_srcs[idx_src].tB;
            if(tA > length || tB > length)
            {
              QDPIO::cerr << "tA = " << tA << ", tB = " << tB << ", length = " << length << std::endl;
              QDP_error_exit("tA or tB > length");
            }

            // FWD prop
            if(prop == 1)
            {
              bool hit_boundary = false;
              for(int t = tA; t != tB+1; ++t)
              {
                if(t == length)
                { 
                  hit_boundary = true;
                  t = 0;
                }

                int t_eff = (t - t0 + length) % length;

                if ( bc_spec < 0 && hit_boundary)
                  barprop[t_eff] = -bardisp1[baryons][sink_mom_num][t];
                else
                  barprop[t_eff] =  bardisp1[baryons][sink_mom_num][t];
              } // for(t)
            } // if(prop == 1)
            // BWD prop
            else if(prop == -1)
            {
              bool hit_boundary = false;
              for(int t = tB; t != tA-1; --t)
              {
                if(t == -1)
                { 
                  hit_boundary = true;
                  t = length-1;
                }

                int t_eff = (t - t0 + length) % length;

                if ( bc_spec < 0 && hit_boundary)
                  barprop[t_eff] =  bardisp1[baryons][sink_mom_num][t];
                else
                  barprop[t_eff] = -bardisp1[baryons][sink_mom_num][t];
              } // for(t)
            } // else if(prop == -1)

          } // for(idx_src)
        } // if(multi_srcs.size() > 0)
        else
        {
          for(int t = 0; t < length; ++t)
          {
            int t_eff = (t - t0 + length) % length;
  
            if ( bc_spec < 0 && (t_eff+t0) >= length)
              barprop[t_eff] = -bardisp1[baryons][sink_mom_num][t];
            else
              barprop[t_eff] =  bardisp1[baryons][sink_mom_num][t];
          }
        } // else of if(multi_srcs.size() > 0)

        write(xml_sink_mom, "barprop", barprop);
        pop(xml_sink_mom);
      } // end for(sink_mom_num)

      pop(xml_sink_mom);
      pop(xml_bar);
    } // end for(gamma_value)

    pop(xml_bar);

    END_CODE();
  }

  //! Nucleon 2-pt functions
  /*!
   * \ingroup hadron
   *
   * This routine is specific to Wilson fermions! 
   *
   * Construct baryon propagators for the Proton with
   * degenerate "u" and "d" quarks.

   * \param propagator_1   "s" quark propagator ( Read )
   * \param propagator_2   "u" quark propagator ( Read )
   * \param t0             cartesian coordinates of the source ( Read )
   * \param bc_spec        boundary condition for spectroscopy ( Read )
   * \param phases         object holds list of momenta and Fourier phases ( Read )
   * \param xml            xml file object ( Read )
   * \param xml_group      group name for xml data ( Read )
   *
   */

  void nucl_2pt_proj(const LatticePropagator& propagator_1, 
      const LatticePropagator& propagator_2, 
      const SftMom& phases,
      int t0, int bc_spec,
      bool axial_proj_only,
      XMLWriter& xml,
      const std::string& xml_group,
      bool cEDM)
  {
      multi1d<SftMomSrcPos_t> multi_srcs;
      multi_srcs.resize(0);

      nucl_2pt_proj(propagator_1, propagator_2, phases,
        t0, multi_srcs, bc_spec, axial_proj_only, xml, xml_group, cEDM);
  }


  //! Heavy-light baryon 2-pt functions
  /*!
   * \ingroup hadron
   *
   * This routine is specific to Wilson fermions! 
   *
   *###########################################################################
   * WARNING: No symmetrization over the spatial part of the wave functions   #
   *          is performed. Therefore, if this routine is called with         #
   *          "shell-sink" quark propagators of different widths the          #
   *          resulting octet baryons may have admixters of excited           #
   *          decouplet baryons with mixed symmetric spatial wave functions,  #
   *          and vice-versa!!!                                               #
   *###########################################################################

   * Construct heavy-light baryon propagators with two "u" quarks and
   * one separate "s" quark for the Sigma^+, the Lambda and the Sigma^{*+}.
   * In the Lambda we take the "u" and "d" quark as degenerate!

   * \param quark_propagator_1   "s" quark propagator ( Read )
   * \param quark_propagator_2   "u" quark propagator ( Read )
   * \param barprop              baryon propagator ( Modify )
   * \param phases               object holds list of momenta and Fourier phases ( Read )
   *
   *        ____
   *        \
   * b(t) =  >  < b(t_source, 0) b(t + t_source, x) >
   *        /                    
   *        ----
   *          x
   */

  void nucl_2pt_proj(const LatticePropagator& quark_propagator_1,
      const LatticePropagator& quark_propagator_2,
      const SftMom& phases,
      multi3d<DComplex>& barprop, bool axial_proj_only, bool cEDM)
  {
    START_CODE();

    // Length of lattice in decay direction
    int length = phases.numSubsets() ;

    if ( Ns != 4 || Nc != 3 )		/* Code is specific to Ns=4 and Nc=3. */
      return;


    // Setup projectors
    int num_projs;
    multi1d<SpinMatrix> projs;
    if(axial_proj_only && !cEDM)
    {
      num_projs = 4;
      projs.resize(num_projs);
      {
        SpinMatrix g_one  =  1.0;
        SpinMatrix g_mone = -1.0;
        projs[0] = Gamma(0 )*g_one; // I
        projs[1] = Gamma(14)*g_one; // gamma_1 gamma_5
        projs[2] = Gamma(13)*g_mone;// gamma_2 gamma_5
        projs[3] = Gamma(11)*g_one; // gamma_3 gamma_5
      }
    }
    else // if cEDM or not axial only
    {
      num_projs = 16;
      projs.resize(num_projs);
      {
        SpinMatrix g_one  =  1.0;
        SpinMatrix g_mone = -1.0;

        projs[ 0] = Gamma( 0) * g_one; ;  //scalar; I
        projs[ 1] = Gamma(14) * g_one; ;  //axial;  g_1 g_5
        projs[ 2] = Gamma(13) * g_mone;;  //axial;  g_2 g_5
        projs[ 3] = Gamma(11) * g_one; ;  //axial;  g_3 g_5
        projs[ 4] = Gamma( 7) * g_mone;;  //axial;  g_4 g_5
        projs[ 5] = Gamma( 1) * g_one; ;  //vector; g_1
        projs[ 6] = Gamma( 2) * g_one; ;  //vector; g_2
        projs[ 7] = Gamma( 4) * g_one; ;  //vector; g_3
        projs[ 8] = Gamma( 8) * g_one; ;  //vector; g_4
        projs[ 9] = Gamma( 9) * g_one; ;  //tensor; g_1 g_4
        projs[10] = Gamma(10) * g_one; ;  //tensor; g_2 g_4
        projs[11] = Gamma(12) * g_one; ;  //tensor; g_3 g_4
        projs[12] = Gamma( 3) * g_one; ;  //tensor; g_1 g_2
        projs[13] = Gamma( 6) * g_one; ;  //tensor; g_2 g_3
        projs[14] = Gamma( 5) * g_one; ;  //tensor; g_1 g_3
        projs[15] = Gamma(15) * g_one; ;  //pseudoscalar; g_5
      }
    } // else of if(axial_proj_only)


    // In general,
    // (1 + gamma_4)/2, NR baryon
    // (1 - gamma_4)/2, NR baryon
    //
    // For CEDM,
    // NR baryon
    // NR baryon with (1 + gamma_4)/2
    // Relativistic baryon
    int num_cases;

    if(cEDM)
      num_cases = 3;
    else
      num_cases = 2;
    
    int num_mom = phases.numMom();
    barprop.resize(num_projs*num_cases, num_mom,length);


    // T_unpol = (1/2)(1 + gamma_4)
    SpinMatrix T_unpol = BaryonSpinMats::Tunpol();

    // T_unpol_NegPar = (1/2)(1 - gamma_4)
    SpinMatrix T_unpol_NegPar = BaryonSpinMats::TunpolNegPar();

    // C gamma_5 = Gamma(5)
    SpinMatrix Cg5 = BaryonSpinMats::Cg5();

    // C g_5 NR = (1/2)*C gamma_5 * ( 1 + g_4 )
    SpinMatrix Cg5NR = BaryonSpinMats::Cg5NR();

    // C g_5 NR negpar = (1/2)*C gamma_5 * ( 1 - g_4 )
    SpinMatrix Cg5NRnegPar = BaryonSpinMats::Cg5NRnegPar();

    LatticeComplex b_prop;
    LatticePropagator di_quark;

    SpinMatrix g_one  =  1.0;

    // Loop over baryons (different projections
    for(int idx_case = 0; idx_case < num_cases; ++idx_case)
    {
      for(int idx_proj = 0; idx_proj < num_projs; ++idx_proj)
      {
        int idx = idx_case * num_projs + idx_proj;

        switch (idx_case)
        {
          case 0:
            // -- Non-relativistic forward propagating proton --
            // Baryon:     (d C gamma_5 (1/2) (1 + gamma_4) u) "u_up"
            // Polarized:  T = (1 + gamma_4) / 2   or   1 (for cEDM)
            // Projection: prj = Gamma(i)
            {
      #if QDP_NC == 3
              SpinMatrix T;
              if(cEDM) T = g_one;
              else     T = T_unpol;

              SpinMatrix sp  = Cg5NR;
              SpinMatrix prj = projs[idx_proj];

              di_quark = quarkContract13(quark_propagator_1 * sp, sp * quark_propagator_2);
              b_prop = LatticeComplex(
                  trace(prj * T * traceColor(quark_propagator_2 * traceSpin(di_quark)))
                + trace(prj * T * traceColor(quark_propagator_2 * di_quark)) );
      #endif
            }
            break;

          case 1:
            if(cEDM){
              // -- Non-relativistic forward propagating proton --
              // Baryon:     (d C gamma_5 (1/2) (1 + gamma_4) u) "u_up"
              // Polarized:  T = (1 + gamma_4) / 2
              // Projection: prj = Gamma(i)
      #if QDP_NC == 3
              SpinMatrix T   = T_unpol;
              SpinMatrix sp  = Cg5NR;
              SpinMatrix prj = projs[idx_proj];

              di_quark = quarkContract13(quark_propagator_1 * sp, sp * quark_propagator_2);
              b_prop = LatticeComplex(
                  trace(prj * T * traceColor(quark_propagator_2 * traceSpin(di_quark)))
                + trace(prj * T * traceColor(quark_propagator_2 * di_quark)) );
      #endif
            }
            // if not calculating cEDM
            else
            {
              // -- Non-relativistic backward propagating proton --
              // Baryon:     (d C gamma_5 (1/2) (1 - gamma_4) u) "u_up"
              // Polarized:  T = (1 - gamma_4) / 2
              // Projection: prj = Gamma(i)
      #if QDP_NC == 3
              SpinMatrix T   = T_unpol_NegPar;
              SpinMatrix sp  = Cg5NRnegPar;
              SpinMatrix prj = projs[idx_proj];

              di_quark = quarkContract13(quark_propagator_1 * sp, sp * quark_propagator_2);
              b_prop = LatticeComplex(
                  trace(prj * T * traceColor(quark_propagator_2 * traceSpin(di_quark)))
                + trace(prj * T * traceColor(quark_propagator_2 * di_quark)) );
      #endif
            }
            break;

          case 2:
            if(cEDM){
              // -- Relativistic forward propagating proton --
              // Baryon:     (d C gamma_5 u) "u_up"
              // Polarized:  T = 1
              // Projection: prj = Gamma(i)
      #if QDP_NC == 3
              SpinMatrix T   = g_one;
              SpinMatrix sp  = Cg5;
              SpinMatrix prj = projs[idx_proj];

              di_quark = quarkContract13(quark_propagator_1 * sp, sp * quark_propagator_2);
              b_prop = LatticeComplex(
                  trace(prj * T * traceColor(quark_propagator_2 * traceSpin(di_quark)))
                + trace(prj * T * traceColor(quark_propagator_2 * di_quark)) );
      #endif
            }
            break;


          default:
            QDP_error_exit("Unknown idx_case", idx_case);

        } // End of switch (idx_case)
      
        // Project onto zero and if desired non-zero momentum
        multi2d<DComplex> hsum;
        hsum = phases.sft(b_prop);
      
        for(int sink_mom_num=0; sink_mom_num < num_mom; ++sink_mom_num) 
          for(int t = 0; t < length; ++t)
          {
            // NOTE: there is NO  1/2  multiplying hsum
            barprop[idx][sink_mom_num][t] = hsum[sink_mom_num][t];
          }

      } //End of idx_proj = [0, num_projs)
    } // End of idx_case = [0, num_cases)

    END_CODE();
  }

}  // end namespace Chroma


