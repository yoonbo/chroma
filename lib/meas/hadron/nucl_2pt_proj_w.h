/*! \file
 *  \brief Nucleon 2-pt functions with projection
 */

#ifndef __nucl_2pt_proj_w_h__
#define __nucl_2pt_proj_w_h__

#include "chromabase.h"
#include "util/ft/sftmom.h"

namespace Chroma 
{
  //! Nucleon 2-pt functions with projections
  /*!
   * \ingroup hadron
   *
   * This routine is specific to Wilson fermions! 
   *
   * Construct baryon propagators for the Proton with
   * degenerate "u" and "d" quarks.

   * \param propagator_1   "s" quark propagator ( Read )
   * \param propagator_2   "u" quark propagator ( Read )
   * \param t0             cartesian coordinates of the source ( Read )
   * \param bc_spec        boundary condition for spectroscopy ( Read )
   * \param phases         object holds list of momenta and Fourier phases ( Read )
   * \param xml            xml file object ( Read )
   * \param xml_group      group name for xml data ( Read )
   * \param cEDM           if measurement for chromo EDM
   *
   */

  void nucl_2pt_proj(const LatticePropagator& propagator_1, 
	       const LatticePropagator& propagator_2, 
	       const SftMom& phases,
	       int t0, multi1d<SftMomSrcPos_t> &multi_srcs, int bc_spec,
         bool axial_proj_only,
	       XMLWriter& xml,
	       const std::string& xml_group,
         bool cEDM);

  void nucl_2pt_proj(const LatticePropagator& propagator_1, 
	       const LatticePropagator& propagator_2, 
	       const SftMom& phases,
	       int t0, int bc_spec,
         bool axial_proj_only,
	       XMLWriter& xml,
	       const std::string& xml_group,
         bool cEDM);


  //! Nucleon 2-pt functions with projections
  /*!
   * \ingroup hadron
   *
   * This routine is specific to Wilson fermions! 
   *
   *###########################################################################
   * WARNING: No symmetrization over the spatial part of the wave functions   #
   *          is performed. Therefore, if this routine is called with         #
   *          "shell-sink" quark propagators of different widths the          #
   *          resulting octet baryons may have admixters of excited           #
   *          decouplet baryons with mixed symmetric spatial wave functions,  #
   *          and vice-versa!!!                                               #
   *                                                                          #
   *###########################################################################

   * Construct nucleon propagators with two "u" quarks and

   * \param quark_propagator_1   "s" quark propagator ( Read )
   * \param quark_propagator_2   "u" quark propagator ( Read )
   * \param barprop              baryon propagator ( Modify )
   * \param phases               object holds list of momenta and Fourier phases ( Read )
   *
   *        ____
   *        \
   * b(t) =  >  < b(t_source, 0) b(t + t_source, x) >
   *        /                    
   *        ----
   *          x
   */

  void nucl_2pt_proj(const LatticePropagator& propagator_1,
	       const LatticePropagator& propagator_2,
	       const SftMom& phases,
	       multi3d<DComplex>& barprop,
         bool axial_proj_only, bool cEDM);

}  // end namespace Chroma


#endif
