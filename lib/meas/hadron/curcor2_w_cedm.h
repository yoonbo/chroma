// -*- C++ -*-
#ifndef __curcor2_cedm_h__
#define __curcor2_cedm_h__

#include "meas/glue/mesfield.h"

namespace Chroma {

//===================================
// Calculate cEDM term
//===================================

void calc_cEDM(const multi1d<LatticeColorMatrix> &u, LatticePropagator &cEDM)
{
  assert(Nd == 4);
  
  multi1d<LatticeColorMatrix> f;
  mesField(f, u);

  // Note that
  // \sigma_{\mu\nu} = i/2 [\gamma_\mu, \gamma_\nu]
  //                 = i gamma_\mu \gamma_\nu  (for \mu \neq \nu)

  // cEDM term is
  //    i * gamma_5 * sigma_{mu nu} * F_{mu nu} (sum over mu<nu)
  // = -2 * gamma_5 * gamma_mu * gamma_nu F_{mu nu}
  // = -2 * 
  //  (- g0 g1 f[5]
  //   + g0 g2 f[4]
  //   - g0 g3 f[3]
  //   - g1 g2 f[2]
  //   + g1 g3 f[1]
  //   - g2 g3 f[0])
  //

  SpinMatrix ms = -1.0;
  SpinMatrix ps =  1.0;
  multi1d<SpinMatrix> gammas(6);
  gammas[5] = ms * Gamma( 3); //g0g1
  gammas[4] = ps * Gamma( 5); //g0g2
  gammas[3] = ms * Gamma( 9); //g0g3
  gammas[2] = ms * Gamma( 6); //g1g2
  gammas[1] = ps * Gamma(10); //g1g3
  gammas[0] = ms * Gamma(12); //g2g3

  cEDM = zero;
  for(int i=0; i<6; ++i)
  {
    // Make identity propagator
    Complex cone = cmplx(Real(1),0);
    ColorMatrix tmp_cmat = zero;
    for (int color = 0; color < Nc; ++color)
      pokeColor(tmp_cmat, cone, color, color); // identity color matrix

    LatticePropagator tmp_prop = zero;
    for (int spin = 0; spin < Ns; ++spin)
      pokeSpin(tmp_prop, tmp_cmat, spin, spin);

    // Also note that the "mesField(f, u)" function calculats i*F_\mu\nu, 
    // so the results from the routine should be multiplied by (-i).

    Complex mimg = cmplx(0,Real(-1));

    // Multiply gamma matrix with gauge field
    cEDM += Real(-2) * mimg * f[i] * (gammas[i] * tmp_prop);
  } // for(int i=0; i<6; ++i)
}

} // end of namespace Chroma
#endif
