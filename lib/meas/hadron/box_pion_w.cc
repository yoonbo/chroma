#include "chromabase.h"
#include "util/ft/sftmom.h"
#include "meas/hadron/box_pion_w.h"
#include "qdp_rannyu.h"
#include <random>

namespace Chroma {

//! Box diagrams for pions
/* 
 * Calculate box diagrams for pions
 *
 * \param list_props  propagator pairs ( Read )
 * \param list_tsrcs  source timeslices of the propagators ( Read )
 * \param T_ins       timeslice of current insertion ( Read )
 * \param N_ins       number of of current insertions ( Read )
 * \param rseed       random seed for random current insertions ( Read )
 * \param IDiagAB     If calculate diag AB ( Read )
 * \param IDiagC     If calculate diag C ( Read )
 * \param xml         xml file object ( Write )
 *
 */

void box_pion(multi2d<const LatticePropagator*> &list_props,
      multi2d<int> &list_tsrcs,
      multi1d<std::string> &out_fnames,
      const int T_ins,
      const int N_ins,
      const int rseed,
      const bool IDiagAB,
      const bool IDiagC,
      const GroupXML_t  &invParam,
      const GroupXML_t  &invParam_1,
      const GroupXML_t  &fermact,
      const bool        &setupParam_1,       
      const multi1d<LatticeColorMatrix> &u,
      XMLWriter& xml)
{
  START_CODE();

  QDPIO::cout << "Entering Box Diagrams for Pions" << std::endl;

  if(list_props.size2() != list_tsrcs.size2())
  {
    QDPIO::cerr << __func__ << ": list_props.size() != list_tsrcs.size(); " << list_props.size2() << "!=" << list_tsrcs.size2() << std::endl;
    QDP_abort(1);
  }

  multi1d<int> latt_size = QDP::Layout::lattSize();

  // Make list of gamma operator combinations
  std::vector<int> gV_list{1,2,4,8};    // vector current: gamma_x, gamma_y, gamma_z, gamma_t
  std::vector<int> gA_list{14,13,11,7}; // axial current: gamma_x g_5, -gamma_y g_5, gamma_z g_5, -gamma_t g_5

  std::vector<std::pair<int,int> > gXY_list;
  for(int i=0; i<gV_list.size(); ++i)
  for(int j=0; j<gA_list.size(); ++j)
    gXY_list.push_back(std::make_pair(gV_list[i], gA_list[j]));

  for(int j=0; j<gA_list.size(); ++j)
  for(int i=0; i<gV_list.size(); ++i)
    gXY_list.push_back(std::make_pair(gA_list[i], gV_list[j]));

  // Make list of random current insertion points
  multi1d<int> cur_ins_list(N_ins*4); // list to save in xml header
  multi2d<int> list_current_ins(N_ins, 4);
  std::mt19937 mt_rand(rseed);
  for(int idx_ins=0; idx_ins<N_ins; idx_ins++)
  {
    // random x,y,z
    for(int mu=0; mu<3; mu++)
      list_current_ins[idx_ins][mu] = mt_rand() % latt_size[mu];

    // given t
    list_current_ins[idx_ins][3] = T_ins;

    for(int mu=0; mu<4; ++mu)
      cur_ins_list[idx_ins*4 + mu] = list_current_ins[idx_ins][mu];
  }//for(idx_ins)

  // FT phases for zero-mom projection
  SftMom phases(0, true, Nd-1);

  //-------------------------------------
  // Calculate Diag A&B
  //-------------------------------------
  // Note that the ideal looping orders for diag A&B and C are different
  if(IDiagAB)
  {
    QDPIO::cout << "Calculating diagrams A & B" << std::endl;

    for(int idx_prop_pair=0; idx_prop_pair<list_props.size2(); ++idx_prop_pair)
    {
      QDPIO::cout << "Calculating diagrams A & B - Propagator pair-" << idx_prop_pair << std::endl;
      QDP::StopWatch swatch;
      swatch.reset();
      swatch.start();

      int N_gamma_comb = gXY_list.size();

      multi1d<LatticeComplex> diagA_avg(N_gamma_comb);
      multi1d<LatticeComplex> diagB_avg(N_gamma_comb);
      multi1d<int> gx_gy_list(N_gamma_comb*2);      // list to save in xml header

      // Bring up propagators and source timeslices
      const LatticePropagator &prop1 = *list_props[idx_prop_pair][0];
      const LatticePropagator &prop2 = *list_props[idx_prop_pair][1];
      int t1 = list_tsrcs[idx_prop_pair][0];
      int t2 = list_tsrcs[idx_prop_pair][1];

      LatticePropagator anti_prop1 = adj(Gamma(15) * prop1 * Gamma(15));
      LatticePropagator anti_prop2 = adj(Gamma(15) * prop2 * Gamma(15));

      // Block: Calculate pion two-point correlators
      Complex Twopt_wp, Twopt_ww;
      {
        LatticeComplex corr_fn = trace(anti_prop1 * (Gamma(15) * prop1 * Gamma(15)));
        multi2d<DComplex> hsum;
        hsum = phases.sft(corr_fn);
        Twopt_wp = hsum[0][t2];
        QDPIO::cout << "Pion wall-to-point twopt:" << Twopt_wp << std::endl;

        multi1d<PropagatorD> summed_prop1 = sumMulti(prop1, phases.getSet());
        Twopt_ww = trace(Gamma(15)*adj(summed_prop1[t2])*Gamma(15)
                              * Gamma(15) * summed_prop1[t2] * Gamma(15));
        QDPIO::cout << "Pion wall-to-wall twopt:" << Twopt_ww << std::endl;
      } // block: calculate 2pt


      swatch.stop();
      QDPIO::cout << "Time(Pion 2pt): " << swatch.getTimeInSeconds() << " s" << std::endl;
      swatch.reset();
      swatch.start();

      double time_shift = 0.0;
      QDP::StopWatch swatch1;

      for(int idx_gx_gy=0; idx_gx_gy<gXY_list.size(); ++idx_gx_gy)
      {
        int gx = gXY_list[idx_gx_gy].first;
        int gy = gXY_list[idx_gx_gy].second;

        diagA_avg[idx_gx_gy] = zero;
        diagB_avg[idx_gx_gy] = zero;

        gx_gy_list[idx_gx_gy*2+0] = gx;
        gx_gy_list[idx_gx_gy*2+1] = gy;

        LatticePropagator y_cont_diagA = Gamma(15) * anti_prop1 * Gamma(gy) * prop2;
        LatticeComplex    y_cont_diagB = trace(Gamma(15) * anti_prop2 * Gamma(gy) * prop2);

        Complex Vector3pt = zero;

        // Loop over X-current positions
        // Final results will be averaged over
        for(int idx_ins=0; idx_ins<N_ins; idx_ins++)
        {
          multi1d<int> xcoord = list_current_ins[idx_ins];

          Real mone = -1;
      
          // Make contractions for all y-positions
          LatticeComplex diagA
            = trace(Gamma(15) * peekSite(anti_prop2, xcoord) * Gamma(gx) * peekSite(prop1, xcoord)
                  * y_cont_diagA);
      
          LatticeComplex diagB
            = mone * trace(Gamma(15) * peekSite(anti_prop1, xcoord) * Gamma(gx) * peekSite(prop1, xcoord))
                   * y_cont_diagB;

          // Shift results relateve to x-position to (0,0,0,0)
          LatticeComplex tmp, diagA_shifted, diagB_shifted;
          diagA_shifted = diagA;
          diagB_shifted = diagB;

          swatch1.reset();
          swatch1.start();
          for(int mu=0; mu<4; ++mu)
          {
            // shift in closest direction (fwd/bwd)
            if(latt_size[mu]-xcoord[mu] < xcoord[mu])
            {
              for(int i_shift=0; i_shift<latt_size[mu]-xcoord[mu]; ++i_shift)
              {
                tmp = shift(diagA_shifted, BACKWARD, mu); diagA_shifted = tmp;
                tmp = shift(diagB_shifted, BACKWARD, mu); diagB_shifted = tmp;
              }
            }
            else
            {
              for(int i_shift=0; i_shift<xcoord[mu]; ++i_shift)
              {
                tmp = shift(diagA_shifted, FORWARD, mu); diagA_shifted = tmp;
                tmp = shift(diagB_shifted, FORWARD, mu); diagB_shifted = tmp;
              }            
            }
          } // for(mu)
          swatch1.stop();
          time_shift += swatch1.getTimeInSeconds();

          diagA_avg[idx_gx_gy] += diagA_shifted;
          diagB_avg[idx_gx_gy] += diagB_shifted;

          // Block: Calculate pion vector charge
          if(idx_gx_gy==0)
          {
            multi1d<PropagatorD> summed_prop2 = sumMulti(prop2, phases.getSet());
            Complex m_gv = trace(Gamma(15) * peekSite(anti_prop2, xcoord) * Gamma(8) 
                                           * peekSite(prop1, xcoord) * Gamma(15) 
                                           * summed_prop2[t1]);
            Vector3pt += m_gv;
          } // Block: Calculate pion vector charge

        }//for(idx_ins) 

        // Average over random insertions
        diagA_avg[idx_gx_gy] /= Real(N_ins);
        diagB_avg[idx_gx_gy] /= Real(N_ins);

        Vector3pt /= Real(N_ins);

        for(int dz=-latt_size[2]/2; dz<latt_size[2]/2; ++dz)
        {
          multi1d<int> ycoord(4);
          ycoord[0] = 0;
          ycoord[1] = 0;
          ycoord[2] = (dz+latt_size[2])%latt_size[2];
          ycoord[3] = 0;

          QDPIO::cout << "Res[g" << gx << "," << gy << "; t" << T_ins << "; dz" << dz << "]: A=" << peekSite(diagA_avg[idx_gx_gy], ycoord)<< " B=" << peekSite(diagB_avg[idx_gx_gy], ycoord) << std::endl;
        }
        if(idx_gx_gy==0)
        {
          QDPIO::cout << "Res[gV4A]: " << Vector3pt << std::endl;
        }

      } // for(idx_gx_gy)
      swatch.stop();
      QDPIO::cout << "Time(Pion 4pt): " << swatch.getTimeInSeconds() << " s (time for shift: " << time_shift << " s)"  << std::endl;
      swatch.reset();
      swatch.start();

      //Block: write to file
      std::vector<std::string> keys{"A", "B"};
      for(std::string key : keys)
      {
        XMLBufferWriter file_xml;
        push(file_xml, "PionBoxDiagram_" + key);
        //write(file_xml, "id", uniqueId());
        pop(file_xml);

        XMLBufferWriter record_xml;
        push(record_xml, "BoxDiag");
        write(record_xml, "DiagType", key);
        write(record_xml, "Version", (int)1);
          push(record_xml, "Twopt");
          write(record_xml, "t1", t1);
          write(record_xml, "t2", t2);
          write(record_xml, "TwoPT_WP", Twopt_wp);
          write(record_xml, "TwoPT_WW", Twopt_ww);
          pop(record_xml);  // Twopt

          push(record_xml, "MeasParams");
          write(record_xml, "t1", t1);
          write(record_xml, "t2", t2);
          write(record_xml, "tins", T_ins);
          write(record_xml, "N_ins", N_ins);
          write(record_xml, "GammaX_GammaY_List", gx_gy_list);
          write(record_xml, "cur_ins_list", cur_ins_list);
          pop(record_xml);  // MeasParams
        pop(record_xml);  // BoxDiag


        std::string fname = out_fnames[idx_prop_pair] + key + ".box";

        // Write the scalar data
        QDPFileWriter to(file_xml, fname,
             QDPIO_SINGLEFILE, QDPIO_PARALLEL, QDPIO_OPEN);
        if(key == "A")
          write(to,record_xml,diagA_avg);
        else if(key == "B")
          write(to,record_xml,diagB_avg);

        close(to);
      } //Block: write to file
      swatch.stop();
      QDPIO::cout << "Time(Save to file): " << swatch.getTimeInSeconds() << " s" << std::endl;
    } // for(idx_prop_pair)
  } // if(IDiagAB)




  //-------------------------------------
  // Calculate Diag C
  //-------------------------------------
  // Note that the ideal looping orders for diag A&B and C are different
  if(IDiagC)
  {
    QDPIO::cout << "Calculating diagrams C" << std::endl;

    QDP::StopWatch swatch;
    swatch.reset();
    swatch.start();

    int N_gamma_comb = gXY_list.size();

    multi2d<LatticeComplex> diagC_avg(list_props.size2(), N_gamma_comb);
    multi1d<int> gx_gy_list(N_gamma_comb*2);      // list to save in xml header

    bool first_run = true;

    LatticePropagator prop_source, xcoord_to_all;
    int ncg_had;

    multi1d<Complex> Twopt_wp(list_props.size2());
    multi1d<Complex> Twopt_ww(list_props.size2());

    multi1d<Complex> Vector3pt(list_props.size2());

    //------------------
    // solver setup
    //------------------
    typedef LatticeFermion               T;
    typedef multi1d<LatticeColorMatrix>  P;
  
    std::istringstream  xml_s(fermact.xml);
    XMLReader  fermacttop(xml_s);
    QDPIO::cout << "FermAct = " << fermact.id << std::endl;
  
    Handle< FermionAction<T,P,P> >
      S_f(TheFermionActionFactory::Instance().createObject(fermact.id,
            fermacttop,
            fermact.path));
  
    Handle< FermState<T,P,P> > state(S_f->createState(u));  

    //------------------------------------
    // Create Point Source
    //------------------------------------
    Propagator PointSrc = zero;
    {
      Real one = 1;
      Complex sitecomp = cmplx(one,0);
      ColorMatrix sitecolor = zero;
      for(int ic = 0; ic < Nc; ++ic)
        pokeColor(sitecolor, sitecomp, ic, ic);

      for(int is = 0; is < Ns; ++is)
        pokeSpin(PointSrc, sitecolor, is, is);
    }
    swatch.stop();
    QDPIO::cout << "Time(Init): " << swatch.getTimeInSeconds() << " s" << std::endl;

    // Loop over X-current positions
    // Final results will be averaged over
    for(int idx_ins=0; idx_ins<N_ins; idx_ins++)
    {
      QDPIO::cout << "Calculating diagrams C insertion-" << idx_ins << std::endl;
      swatch.reset();
      swatch.start();

      multi1d<int> xcoord = list_current_ins[idx_ins];

      //------------------------------------------------
      // Calculate point-source propagator from xcoord
      //------------------------------------------------
      // Point source at xcoord
      prop_source = zero;
      Propagator m_ps = PointSrc;
      pokeSite(prop_source, m_ps, xcoord);

      xcoord_to_all = zero;
      GroupXML_t invp;

      if(setupParam_1 && first_run)
      {
        invp = invParam_1;
        first_run = false;
      }
      else
      {
        invp = invParam;
      }
  
      XMLFileWriter xml_out;

      S_f->quarkProp(xcoord_to_all,
        xml_out,
        prop_source,
        xcoord[3], // t0
        Nd-1, // jdecay
        state,
        invp, // invParam
        QUARK_SPIN_TYPE_FULL, // quarkSpinType
        false, // obsvP
        ncg_had);

      swatch.stop();
      QDPIO::cout << "Time(Point source propagator): " << swatch.getTimeInSeconds() << " s" << std::endl;
      swatch.reset();
      swatch.start();

      double time_shift = 0.0;
      QDP::StopWatch swatch1;

      // Loop over propagator pairs
      for(int idx_prop_pair=0; idx_prop_pair<list_props.size2(); ++idx_prop_pair)
      {
        // Bring up propagators and source timeslices
        const LatticePropagator &prop1 = *list_props[idx_prop_pair][0];
        const LatticePropagator &prop2 = *list_props[idx_prop_pair][1];
        int t1 = list_tsrcs[idx_prop_pair][0];
        int t2 = list_tsrcs[idx_prop_pair][1];
  
        //LatticePropagator anti_prop1 = adj(Gamma(15) * prop1 * Gamma(15));
        LatticePropagator anti_prop2 = adj(Gamma(15) * prop2 * Gamma(15));
        //multi1d<PropagatorD> summed_prop1 = sumMulti(prop1, phases.getSet());
        multi1d<PropagatorD> summed_prop2 = sumMulti(prop2, phases.getSet());

        LatticePropagator y_cont_diagC = Gamma(15)* summed_prop2[t1] * Gamma(15) * anti_prop2;

        // Block: Calculate pion two-point correlators
        if(idx_ins == 0)
        {
          LatticePropagator anti_prop1 = adj(Gamma(15) * prop1 * Gamma(15));
          multi1d<PropagatorD> summed_prop1 = sumMulti(prop1, phases.getSet());
          
          LatticeComplex corr_fn = trace(anti_prop1 * (Gamma(15) * prop1 * Gamma(15)));
          multi2d<DComplex> hsum;
          hsum = phases.sft(corr_fn);
          Twopt_wp[idx_prop_pair] = hsum[0][t2];
          QDPIO::cout << "Pion wall-to-point twopt:" << Twopt_wp[idx_prop_pair] << std::endl;

          Twopt_ww[idx_prop_pair] = trace(Gamma(15)*adj(summed_prop1[t2])*Gamma(15)
                                * Gamma(15) * summed_prop1[t2] * Gamma(15));
          QDPIO::cout << "Pion wall-to-wall twopt:" << Twopt_ww[idx_prop_pair] << std::endl;
        } // block: calculate 2pt

        for(int idx_gx_gy=0; idx_gx_gy<gXY_list.size(); ++idx_gx_gy)
        {
          int gx = gXY_list[idx_gx_gy].first;
          int gy = gXY_list[idx_gx_gy].second;

          LatticePropagator prop1_to_all = xcoord_to_all * Gamma(gx) * peekSite(prop1, xcoord);
  
          if(idx_ins == 0)
          {
            diagC_avg[idx_prop_pair][idx_gx_gy] = zero;
            gx_gy_list[idx_gx_gy*2+0] = gx;
            gx_gy_list[idx_gx_gy*2+1] = gy;

            if(idx_gx_gy == 0)
              Vector3pt[idx_prop_pair] = zero;
          }
  
          //--------------------------------------------------
          // Calculate (wall1 to vector current to all) prop
          //--------------------------------------------------
          LatticeComplex diagC = trace(y_cont_diagC * Gamma(gy) * prop1_to_all );

          // Shift results relateve to x-position to (0,0,0,0)
          LatticeComplex tmp, diagC_shifted;
          diagC_shifted = diagC;

          swatch1.reset();
          swatch1.start();
          for(int mu=0; mu<4; ++mu)
          {
            // shift in closest direction (fwd/bwd)
            if(latt_size[mu]-xcoord[mu] < xcoord[mu])
            {
              for(int i_shift=0; i_shift<latt_size[mu]-xcoord[mu]; ++i_shift)
              {
                tmp = shift(diagC_shifted, BACKWARD, mu); diagC_shifted = tmp;
              }
            }
            else
            {
              for(int i_shift=0; i_shift<xcoord[mu]; ++i_shift)
              {
                tmp = shift(diagC_shifted, FORWARD, mu); diagC_shifted = tmp;
              }            
            }
          } // for(mu)
          swatch1.stop();
          time_shift += swatch1.getTimeInSeconds();

          diagC_avg[idx_prop_pair][idx_gx_gy] += diagC_shifted;

          // Block: Calculate pion vector charge
          if(idx_gx_gy == 0)
          {
            LatticePropagator mprop1_to_all = xcoord_to_all * Gamma(8) * peekSite(prop1, xcoord);
            multi1d<PropagatorD> summed_mprop1_to_all = sumMulti(mprop1_to_all, phases.getSet());

            multi1d<PropagatorD> summed_prop2 = sumMulti(prop2, phases.getSet());
            Complex m_gv = trace(Gamma(15) * summed_mprop1_to_all[t2] * Gamma(15) * summed_prop2[t1]);

            Vector3pt[idx_prop_pair] += m_gv;
          } // Block: Calculate pion vector charge


        }//for(idx_gx_gy)
      } // for(idx_prop_pair)

      swatch.stop();
      QDPIO::cout << "Time(4pt contractions): " << swatch.getTimeInSeconds() << " s (time for shift: " << time_shift << " s)"  << std::endl;
    } // for(idx_ins)

    //------------------------------------------
    // Average over random insertions and save
    //------------------------------------------
    for(int idx_prop_pair=0; idx_prop_pair<list_props.size2(); ++idx_prop_pair)
    for(int idx_gx_gy=0; idx_gx_gy<gXY_list.size(); ++idx_gx_gy)
    {
      int gx = gXY_list[idx_gx_gy].first;
      int gy = gXY_list[idx_gx_gy].second;

      diagC_avg[idx_prop_pair][idx_gx_gy] /= Real(N_ins);

      for(int dz=-latt_size[2]/2; dz<latt_size[2]/2; ++dz)
      {
        multi1d<int> ycoord(4);
        ycoord[0] = 0;
        ycoord[1] = 0;
        ycoord[2] = (dz+latt_size[2])%latt_size[2];
        ycoord[3] = 0;

        QDPIO::cout << "Res[g" << gx << "," << gy << "; t" << T_ins << "; dz" << dz << "]: C=" << peekSite(diagC_avg[idx_prop_pair][idx_gx_gy], ycoord) << std::endl;
      }

      if(idx_gx_gy==0)
      {
        QDPIO::cout << "Res[gV4C]: " << Vector3pt[idx_prop_pair]/Real(N_ins) << std::endl;
      }

    } // for(idx_prop_pair) for(idx_gx_gy)

    swatch.reset();
    swatch.start();
    // save to file
    for(int idx_prop_pair=0; idx_prop_pair<list_props.size2(); ++idx_prop_pair)
    {
      int t1 = list_tsrcs[idx_prop_pair][0];
      int t2 = list_tsrcs[idx_prop_pair][1];

      std::vector<std::string> keys{"C"};
      for(std::string key : keys)
      {
        XMLBufferWriter file_xml;
        push(file_xml, "PionBoxDiagram_" + key);
        //write(file_xml, "id", uniqueId());
        pop(file_xml);

        XMLBufferWriter record_xml;
        push(record_xml, "BoxDiag");
        write(record_xml, "DiagType", key);
        write(record_xml, "Version", (int)1);
          push(record_xml, "Twopt");
          write(record_xml, "t1", t1);
          write(record_xml, "t2", t2);
          write(record_xml, "TwoPT_WP", Twopt_wp[idx_prop_pair]);
          write(record_xml, "TwoPT_WW", Twopt_ww[idx_prop_pair]);
          pop(record_xml);  // Twopt

          push(record_xml, "MeasParams");
          write(record_xml, "t1", t1);
          write(record_xml, "t2", t2);
          write(record_xml, "tins", T_ins);
          write(record_xml, "N_ins", N_ins);
          write(record_xml, "GammaX_GammaY_List", gx_gy_list);
          write(record_xml, "cur_ins_list", cur_ins_list);
          pop(record_xml);  // MeasParams
        pop(record_xml);  // BoxDiag

        std::string fname = out_fnames[idx_prop_pair] + key + ".box";

        // Write the scalar data
        QDPFileWriter to(file_xml, fname,
             QDPIO_SINGLEFILE, QDPIO_PARALLEL, QDPIO_OPEN);
        if(key == "C")
          write(to,record_xml,diagC_avg[idx_prop_pair]);

        close(to);
      } //Block: write to file
    } // for(idx_prop_pair)
    swatch.stop();
    QDPIO::cout << "Time(Save all prop pair results to file): " << swatch.getTimeInSeconds() << " s" << std::endl;

  } // if(IDiagC)


} // void box_pion(...)

}  // end namespace Chroma

