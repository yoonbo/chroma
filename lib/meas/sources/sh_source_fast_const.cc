// $Id: sh_source_const.cc,v 3.14 2008-11-04 18:43:59 edwards Exp $
/*! \file
 *  \brief Shell source construction
 */

// -- WARNING!! --------------------------------------------
//  This code is not tested for nonzero Quark Displacements
//  This code is not implemented for Staggered Fermion 
// ---------------------------------------------------------

#include "chromabase.h"
#include "handle.h"

#include "meas/sources/source_const_factory.h"
#include "meas/sources/sh_source_fast_const.h"
#include "meas/sources/srcfil.h"
#include "util/ferm/transf.h"

#include "meas/smear/quark_smearing_factory.h"
#include "meas/smear/quark_smearing_aggregate.h"

#include "meas/smear/link_smearing_aggregate.h"
#include "meas/smear/link_smearing_factory.h"

#include "meas/smear/quark_displacement_aggregate.h"
#include "meas/smear/quark_displacement_factory.h"

#include "meas/smear/simple_quark_displacement.h"
#include "meas/smear/no_quark_displacement.h"

namespace Chroma
{
  // Read parameters
  void read(XMLReader& xml, const std::string& path, ShellQuarkSourceFastConstEnv::Params& param)
  {
    ShellQuarkSourceFastConstEnv::Params tmp(xml, path);
    param = tmp;
  }

  // Writer
  void write(XMLWriter& xml, const std::string& path, const ShellQuarkSourceFastConstEnv::Params& param)
  {
    param.writeXML(xml, path);
  }



  //! Hooks to register the class
  namespace ShellQuarkSourceFastConstEnv
  {
    namespace TSF
    {
      // Standard Time Slicery
      class TimeSliceFunc : public SetFunc
      {
        public:
          TimeSliceFunc(int dir): dir_decay(dir) {}
          int operator() (const multi1d<int>& coordinate) const {return coordinate[dir_decay];}
          int numSubsets() const {return Layout::lattSize()[dir_decay];}
          int dir_decay;
        private:
          TimeSliceFunc() {}  // hide default constructor
      };
    }

    namespace
    {
      //! Callback function
      QuarkSourceConstruction<LatticePropagator>* createProp(XMLReader& xml_in,
          const std::string& path)
      {
        return new SourceConst<LatticePropagator>(Params(xml_in, path));
      }

//  Not implemented for Staggered fermion
//      //! Callback function
//      QuarkSourceConstruction<LatticeStaggeredPropagator>* createStagProp(XMLReader& xml_in,
//          const std::string& path)
//      {
//        return new SourceConst<LatticeStaggeredPropagator>(Params(xml_in, path));
//      }

      //! Local registration flag
      bool registered = false;

      //! Name to be used
      const std::string name("SHELL_SOURCE_FAST");
    }

    //! Return the name
    std::string getName() {return name;}

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
        success &= LinkSmearingEnv::registerAll();
        success &= QuarkSmearingEnv::registerAll();
        success &= QuarkDisplacementEnv::registerAll();
        success &= Chroma::ThePropSourceConstructionFactory::Instance().registerObject(name, createProp);
//        success &= Chroma::TheStagPropSourceConstructionFactory::Instance().registerObject(name, createStagProp);

        registered = true;
      }
      return success;
    }


    //! Read parameters
    Params::Params()
    {
      j_decay = -1;
      t_srce.resize(Nd);
      t_srce = 0;
      quark_smear_lastP = true;
    }

    //! Read parameters
    Params::Params(XMLReader& xml, const std::string& path)
    {
      XMLReader paramtop(xml, path);

      int version;
      read(paramtop, "version", version);

      quark_smear_lastP = true;

      switch (version) 
      {
        case 1:
          {
            quark_displacement = QuarkDisplacementEnv::nullXMLGroup();
            quark_displacement.id = SimpleQuarkDisplacementEnv::getName();
            int disp_length = 0;
            int disp_dir = 0;

            XMLBufferWriter xml_tmp;
            push(xml_tmp, "Displacement");
            write(xml_tmp, "DisplacementType", quark_displacement.id);

            if (paramtop.count("disp_length") != 0)
              read(paramtop, "disp_length", disp_length);

            if (paramtop.count("disp_dir") != 0)
              read(paramtop, "disp_dir", disp_dir);

            write(xml_tmp, "disp_length", disp_length);
            write(xml_tmp, "disp_dir",  disp_dir);

            pop(xml_tmp);  // Displacement

            quark_displacement.xml = xml_tmp.printCurrentContext();
          }
          break;

        case 2:
          {
            if (paramtop.count("Displacement") != 0)
              quark_displacement = readXMLGroup(paramtop, "Displacement", "DisplacementType");
            else
              quark_displacement = QuarkDisplacementEnv::nullXMLGroup();
          }
          break;

        case 3:
          {
            read(paramtop, "quark_smear_lastP", quark_smear_lastP);

            if (paramtop.count("Displacement") != 0)
              quark_displacement = readXMLGroup(paramtop, "Displacement", "DisplacementType");
            else
              quark_displacement = QuarkDisplacementEnv::nullXMLGroup();
          }
          break;

        default:
          QDPIO::cerr << __func__ << ": parameter version " << version 
            << " unsupported." << std::endl;
          QDP_abort(1);
      }

      quark_smearing = readXMLGroup(paramtop, "SmearingParam", "wvf_kind");

      if (paramtop.count("LinkSmearing") != 0)
        link_smearing = readXMLGroup(paramtop, "LinkSmearing", "LinkSmearingType");
      else
        link_smearing = LinkSmearingEnv::nullXMLGroup();

      read(paramtop, "t_srce", t_srce);
      read(paramtop, "j_decay",  j_decay);
    }


    // Writer
    void Params::writeXML(XMLWriter& xml, const std::string& path) const
    {
      push(xml, path);

      int version = 3;
      QDP::write(xml, "version", version);

      write(xml, "SourceType", ShellQuarkSourceFastConstEnv::name);
      xml << quark_smearing.xml;
      xml << quark_displacement.xml;
      xml << link_smearing.xml;

      write(xml, "t_srce",  t_srce);
      write(xml, "j_decay",  j_decay);
      write(xml, "quark_smear_lastP",  quark_smear_lastP);

      pop(xml);
    }



    //! Construct the source
    template<>
    LatticePropagator
    SourceConst<LatticePropagator>::operator()(const multi1d<LatticeColorMatrix>& u) const
    {
      QDPIO::cout << "Shell source" << std::endl;

      LatticePropagator quark_source;

      try
      {
        // Machinery to do timeslice replication with
        Set TS;
        TS.make(TSF::TimeSliceFunc(Nd-1));

        int j_decay = params.j_decay;
        int t_src   = params.t_srce[j_decay];

        //
        // Smear the gauge field if needed
        //
        multi1d<LatticeColorMatrix> u_smr = u;
        {
          std::istringstream  xml_l(params.link_smearing.xml);
          XMLReader  linktop(xml_l);
          QDPIO::cout << "Link smearing type = " << params.link_smearing.id << std::endl;

          Handle< LinkSmearing >
            linkSmearing(TheLinkSmearingFactory::Instance().createObject(params.link_smearing.id,
                  linktop,
                  params.link_smearing.path));
          (*linkSmearing)(u_smr);
        }

        //--------------------------------------------------
        // Make gauge field that has duplicated timeslices
        //--------------------------------------------------
        multi1d<LatticeColorMatrix> u_smr_d(Nd);
        for(int mu=0; mu<Nd; ++mu)  u_smr_d[mu] = zero;

        // block: Make gauge field that has duplicated timeslices
        {
          // Check j_decay
          if(j_decay != 3 && Nd != 4)
          {
            QDPIO::cerr << __func__ << ": j_decay " << j_decay
              << ", Nd " << Nd
              << " unsupported." << std::endl;
            QDP_abort(1);
          }

          // Gauge links only for one timeslice
          multi1d<LatticeColorMatrix> u_smr_1(Nd);
          for(int mu=0; mu<Nd; ++mu)  u_smr_1[mu] = zero;

          // Loop over \mu in gauge link
          for(int mu=0; mu<Nd; ++mu)
          {
            // Do not need mu in time direction for the klein_gord
            if(mu == j_decay) continue;

            // Replicate one timeslice to u_smr_1
            u_smr_1[mu][TS[t_src]] = u_smr[mu];
          }

          // Gauge links whose 12 timeslices are the t_sink.
          // They are copied to t=t_src, (t_src+1)%LT, ..., (t_src+11)%LT
          u_smr_d = u_smr_1;
          LatticeColorMatrix tmp_lcm;
          for(int mu=0; mu<Nd; ++mu)
          {
            // Do not need mu in time direction for the klein_gord
            if(mu == j_decay) continue;

            for(int virtual_t_src=1; virtual_t_src<12; ++virtual_t_src)
            {
              tmp_lcm = shift(u_smr_1[mu],BACKWARD,j_decay);
              u_smr_1[mu] = tmp_lcm;
              u_smr_d[mu]+= u_smr_1[mu];
            }
          }
        } // End of block: Make gauge field that has duplicated timeslices

        //
        // Create the quark smearing object
        //
        std::istringstream  xml_s(params.quark_smearing.xml);
        XMLReader  smeartop(xml_s);
        QDPIO::cout << "Quark smearing type = " << params.quark_smearing.id << std::endl;

        Handle< QuarkSmearing<LatticeFermion> >
          quarkSmearing(TheFermSmearingFactory::Instance().createObject(params.quark_smearing.id,
          smeartop,
          params.quark_smearing.path));

        //
        // Create the quark displacement object
        //
        std::istringstream  xml_d(params.quark_displacement.xml);
        XMLReader  displacetop(xml_d);
        QDPIO::cout << "Displacement type = " << params.quark_displacement.id << std::endl;

        // This code is not tested for non-zero Quark Displacement
        if(params.quark_displacement.id != "NONE")
          QDPIO::cerr << "This smearing code is not test for non-zero Quark Displacment!!" << std::endl;

        Handle< QuarkDisplacement<LatticeFermion> >
          quarkDisplacement(TheFermDisplacementFactory::Instance().createObject(params.quark_displacement.id,
          displacetop,
          params.quark_displacement.path));


        // block: Create quark source and do smearing
        {
          //----------------------
          // Create quark source
          //----------------------
          LatticeFermion chi_v = zero;
          multi1d<int> virtual_src_pos = params.t_srce;
          int N_T  = Layout::lattSize()[j_decay];

          // Place sources into virtual 12 timeslices for 
          // 12 color and spin components
          // first spin/color component is at t_src, next one is at (t_src+1)%N_T, ...
          for(int color_source = 0; color_source < Nc; ++color_source)
          for(int spin_source = 0; spin_source < Ns; ++spin_source)
          {
            int virtual_t_src = (t_src + color_source*Ns + spin_source)%N_T;
            virtual_src_pos[j_decay] = virtual_t_src;
            srcfil(chi_v, virtual_src_pos, color_source, spin_source);
          }

          //----------------------
          // Quark displacement 
          //----------------------
          // Quark displacement first, and do the smearing  
          if (params.quark_smear_lastP)
            (*quarkDisplacement)(chi_v, u_smr_d, MINUS);

          //----------------------
          // Smearing 
          //----------------------
          // do the smearing with virtual gauge field, and with duplicated timeslices
          (*quarkSmearing)(chi_v, u_smr_d);

          //----------------------
          // Quark displacement 
          //----------------------
          // Smearing first, and do the quark displacement
          if (!params.quark_smear_lastP)
            (*quarkDisplacement)(chi_v, u_smr_d, MINUS);

          LatticeFermion tmp_lf;
          // Convert the smeared source into LatticePropagator form
          // so that it can be read from inverter
          for(int color_source = 0; color_source < Nc; ++color_source)
          for(int spin_source = 0; spin_source < Ns; ++spin_source)
          {
            LatticeFermion chi = zero; // smeared source with right timeslice

            // Here chi is the smeared source for 12 color and spin choices.
            // They are located at t=t_src, t_src+1, ..., t_src+11, and needs 
            // to be shifted to the source-insertion-timeslice, target_t_src.
            chi[TS[t_src]] = chi_v;

            // Move the smeared source to appropriate position of quark source
            FermToProp(chi, quark_source, color_source, spin_source);

            // shift chi_v so that next source be at t_src
            if(color_source!=Nc-1 || spin_source!=Ns-1)
            {
              tmp_lf = shift(chi_v, FORWARD, j_decay);
              chi_v = tmp_lf;
            }
          }
        }// End block: Create quark source and do smearing

      }
      catch(const std::string& e) 
      {
        QDPIO::cerr << name << ": Caught Exception smearing: " << e << std::endl;
        QDP_abort(1);
      }

      return quark_source;
    }
  }
}
