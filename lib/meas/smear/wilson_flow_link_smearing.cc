/*! \file
 *  \brief Wilson Flow link smearing
 */

#include "chromabase.h"

#include "meas/smear/link_smearing_factory.h"
#include "meas/smear/wilson_flow_link_smearing.h"
#include "meas/glue/wilson_flow_w.h"

namespace Chroma
{

  // Read parameters
  void read(XMLReader& xml, const std::string& path, WilsonFlowLinkSmearingEnv::Params& param)
  {
    WilsonFlowLinkSmearingEnv::Params tmp(xml, path);
    param = tmp;
  }

  //! Parameters for running code
  void write(XMLWriter& xml, const std::string& path, const WilsonFlowLinkSmearingEnv::Params& param)
  {
    param.writeXML(xml, path);
  }


  //! Hooks to register the class
  namespace WilsonFlowLinkSmearingEnv
  {
    namespace
    {
      //! Callback function
      LinkSmearing* createSource(XMLReader& xml_in,
				 const std::string& path)
      {
	return new LinkSmear(Params(xml_in, path));
      }

      //! Local registration flag
      bool registered = false;

      //! Name to be used
      const std::string name = "WILSON_FLOW_SMEAR";
    }

    //! Return the name
    std::string getName() {return name;}

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
	success &= Chroma::TheLinkSmearingFactory::Instance().registerObject(name, createSource);
	registered = true;
      }
      return success;
    }


    //! Parameters for running code
    Params::Params(XMLReader& xml, const std::string& path)
    {
      XMLReader paramtop(xml, path);

      int version = 1;
      if (paramtop.count("version") > 0)
	read(paramtop, "version", version);

      switch (version) 
      {
      case 1:
      {
      }
      break;

      default:
	QDPIO::cerr << WilsonFlowLinkSmearingEnv::name << ": Input version " << version 
		    << " unsupported." << std::endl;
	QDP_abort(1);
      }

    	read(paramtop, "wtime", wtime);
    	read(paramtop, "nstep", nstep);

    }


    //! Parameters for running code
    void Params::writeXML(XMLWriter& xml, const std::string& path) const
    {
      push(xml, path);
    
      int version = 1;
      write(xml, "version", version);
      write(xml, "LinkSmearingType", WilsonFlowLinkSmearingEnv::name);
      write(xml, "wtime", wtime);
      write(xml, "nstep", nstep);

      pop(xml);
    }


    //! Smear the links
    void
    LinkSmear::operator()(multi1d<LatticeColorMatrix>& u) const
    {
      // Now WF smear

      if (params.nstep > 0)
      {
	QDPIO::cout << "WilsonFlow Smear gauge field" << std::endl;

  Real wflow_eps = params.wtime/params.nstep;

  for(int i=0 ; i < params.nstep ; ++i)
  {
    wilson_flow_one_step(u,wflow_eps) ;
  }

	QDPIO::cout << "Gauge field WilsonFlow-smeared!" << std::endl;
      }

    }
  }
}
