// -*- C++ -*-
/*! \file
 *  \brief Wilson Flow 
 */

#ifndef __wilson_flow_link_smearing_h__
#define __wilson_flow_link_smearing_h__

#include "meas/smear/link_smearing.h"

namespace Chroma
{

  //! Name and registration
  namespace WilsonFlowLinkSmearingEnv
  {
    bool registerAll();

    //! Return the name
    std::string getName();

    //! Params for WilsonFlow link smearing
    /*! @ingroup smear */
    struct Params
    {
      Params() {}
      Params(XMLReader& in, const std::string& path);
      void writeXML(XMLWriter& in, const std::string& path) const;
    
      Real           wtime;    /*!< total flow time */
      int            nstep;    /*!< Number of steps (eps = wtime/nstep) */
    };



    //! WilsonFlow link smearing
    /*! @ingroup smear
     *
     * WilsonFlow link smearing object
     */
    class LinkSmear : public LinkSmearing
    {
    public:
      //! Full constructor
      LinkSmear(const Params& p) : params(p) {}

      //! Smear the links
      void operator()(multi1d<LatticeColorMatrix>& u) const;

    private:
      //! Hide partial constructor
      LinkSmear() {}

    private:
      Params  params;   /*!< smearing params */
    };

  }  // end namespace


  //! Reader
  /*! @ingroup smear */
  void read(XMLReader& xml, const std::string& path, WilsonFlowLinkSmearingEnv::Params& param);

  //! Writer
  /*! @ingroup smear */
  void write(XMLWriter& xml, const std::string& path, const WilsonFlowLinkSmearingEnv::Params& param);

}  // end namespace Chroma


#endif
