// -*- C++ -*-
/*! \file
 * \brief Inline Wilson flow propagators
 *
 * Wilson flow propagators
 */

#ifndef __inline_wilson_flow_props_w_h__
#define __inline_wilson_flow_props_w_h__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "io/qprop_io.h"

namespace Chroma 
{ 
  /*! \ingroup inlinehadron */
  namespace InlineWilsonFlowPropsEnv 
  {
    extern const std::string name;
    bool registerAll();

    //! Parameter structure
    /*! \ingroup inlinehadron */
    struct Params
    {
      Params();
      Params(XMLReader& xml_in, const std::string& path);
      void writeXML(XMLWriter& xml_out, const std::string& path);

      unsigned long   frequency;

      struct WilsonFlow_t
      {
        Real   wtime; /*!< total flow time */
        Real   weps;  /*!< eps for Wilson flow */
        GroupXML_t   cfs; /*!< Fermion state */
        std::string BkwdPropG5Format;
      } wflow_params;

      struct NamedObject_t
      {
	std::string   gauge_id;
	std::string   flowed_gauge_id;
	multi1d<std::string>   prop_ids;
	multi1d<std::string>   seqprop_ids;
	multi1d<std::string>   flowed_prop_ids;
	multi1d<std::string>   flowed_seqprop_ids;
      } named_obj;
    };


    //! Inline task for Wilson flow propagators
    /*! \ingroup inlinehadron */
    class InlineMeas : public AbsInlineMeasurement 
    {
    public:
      ~InlineMeas() {}
      InlineMeas(const Params& p) : params(p) {}
      InlineMeas(const InlineMeas& p) : params(p.params) {}

      unsigned long getFrequency(void) const {return params.frequency;}

      //! Do the measurement
      void operator()(const unsigned long update_no,
		      XMLWriter& xml_out); 

    protected:
      //! Run Wilson flow
      void func(multi1d<LatticeColorMatrix> & u, Real wtime, Real weps, GroupXML_t cfs,
          multi1d<LatticePropagator> &props);

    private:
      Params params;
    };

  }

}

#endif
