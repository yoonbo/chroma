/*! \file
 * \brief Inline two-point function for the baryon disconnected diagram calculations
 *
 * Two-point function for the baryon disconnected diagram calculations
 */

#ifndef __inline_nucl_2pt_proj_h__
#define __inline_nucl_2pt_proj_h__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "util/ft/sftmom.h"

namespace Chroma 
{ 
  /*! \ingroup inlinehadron */
  namespace InlineNuclTwoptProjEnv 
  {
    extern const std::string name;
    bool registerAll();
  }

  //! Parameter structure
  /*! \ingroup inlinehadron */
  struct InlineNuclTwoptProjParams 
  {
    InlineNuclTwoptProjParams();
    InlineNuclTwoptProjParams(XMLReader& xml_in, const std::string& path);
    void write(XMLWriter& xml_out, const std::string& path);

    unsigned long frequency;

    struct Param_t
    {
      int version;

      int mom2_max;            // (mom)^2 <= mom2_max. mom2_max=7 in szin.
      bool avg_equiv_mom;      // average over equivalent momenta
      bool cEDM;               // measurement for cEDM

      bool axial_proj_only;      // project only to axial

      bool multi_srcsP;        // Multiple source positions
      multi1d<SftMomSrcPos_t> multi_srcs;  // Source positions
      // SftMomSrcPos_t is defined in lib/util/ft/sftmom.h
    } param;

    struct NamedObject_t
    {
      std::string  gauge_id;           /*!< Input gauge field */

      struct Props_t
      {
        std::string  first_id;
        std::string  second_id;
      };

      multi1d<Props_t> sink_pairs;
    } named_obj;

    std::string xml_file;  // Alternate XML file pattern
  };


  class InlineNuclTwoptProj : public AbsInlineMeasurement 
  {
    public:
      ~InlineNuclTwoptProj() {}
      InlineNuclTwoptProj(const InlineNuclTwoptProjParams& p) : params(p) {}
      InlineNuclTwoptProj(const InlineNuclTwoptProj& p) : params(p.params) {}

      unsigned long getFrequency(void) const {return params.frequency;}

      //! Do the measurement
      void operator()(const unsigned long update_no,
          XMLWriter& xml_out); 

    protected:
      //! Do the measurement
      void func(const unsigned long update_no,
          XMLWriter& xml_out); 

    private:
      InlineNuclTwoptProjParams params;
  };

};

#endif
