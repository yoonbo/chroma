/*! \file
 * \brief Inline Box diagram calculation
 *
 * Box diagram calculation
 */

// Code structure is taken from inline_hadspec_w.cc

#include "meas/inline/hadron/inline_box_w.h"
#include "meas/inline/abs_inline_measurement_factory.h"
#include "util/ft/sftmom.h"
#include "util/info/proginfo.h"
#include "io/param_io.h"
#include "io/qprop_io.h"
#include "meas/hadron/box_pion_w.h"
#include "meas/inline/make_xml_file.h"
#include "meas/inline/io/named_objmap.h"

namespace Chroma 
{ 
  namespace InlineBoxEnv 
  { 
    namespace
    {
      AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
          const std::string& path) 
      {
        return new InlineBox(InlineBoxParams(xml_in, path));
      }

      //! Local registration flag
      bool registered = false;
    }

    const std::string name = "BOX";

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
        success &= TheInlineMeasurementFactory::Instance().registerObject(name, createMeasurement);
        registered = true;
      }
      return success;
    }
  }



  //! Reader for parameters
  void read(XMLReader& xml, const std::string& path, InlineBoxParams::Param_t& param)
  {
    XMLReader paramtop(xml, path);

    int version;
    read(paramtop, "version", version);

    switch (version) 
    {
      case 1:
        break;

      default:
        QDPIO::cerr << "Input parameter version " << version << " unsupported." << std::endl;
        QDP_abort(1);
    }

    read(paramtop, "PionP", param.PionP);
//    read(paramtop, "KaonP", param.KaonP);
//    read(paramtop, "NeutronP", param.NeutronP);
    param.KaonP = false;
    param.NeutronP = false;

//    read(paramtop, "mom2_max", param.mom2_max);

    if (paramtop.count("DiagAB") == 1)
      read(paramtop, "DiagAB", param.DiagAB);
    else
      param.DiagAB = false;

    if (paramtop.count("DiagC") == 1)
      read(paramtop, "DiagC", param.DiagC);
    else
      param.DiagC = false;

    read(paramtop, "T_ins", param.T_ins);
    read(paramtop, "N_ins", param.N_ins);
    read(paramtop, "rseed", param.rseed);

    // if DiagC is activated, read inversion parameters
    if(param.DiagC)
    {
      param.invParam = readXMLGroup(paramtop, "InvertParam", "invType");
      param.fermact = readXMLGroup(paramtop, "FermionAction", "FermAct");

      // Some inverters, such as Multigrid, may require differnt parameters for its first run
      if (paramtop.count("InvertParam_1") > 0) {
        param.invParam_1 = readXMLGroup(paramtop, "InvertParam_1", "invType");
        param.setupParam_1 = true;
      }
      else {
        param.setupParam_1 = false;
      }
    } // if(param.DiagC)
  }


  //! Writer for parameters
  void write(XMLWriter& xml, const std::string& path, const InlineBoxParams::Param_t& param)
  {
    push(xml, path);

    int version = 1;
    write(xml, "version", version);

    write(xml, "PionP", param.PionP);
//    write(xml, "KaonP", param.KaonP);
//    write(xml, "NeutronP", param.NeutronP);

//    write(xml, "mom2_max", param.mom2_max);

    write(xml, "DiagAB", param.DiagAB);
    write(xml, "DiagC",  param.DiagC);

    write(xml, "T_ins", param.T_ins);
    write(xml, "N_ins", param.N_ins);
    write(xml, "rseed", param.rseed);

    push(xml, "InvertParam");
    xml << param.invParam.xml;
    pop(xml);

    push(xml, "InvertParam_1");
    xml << param.invParam_1.xml;
    pop(xml);

    push(xml, "FermionAction");
    xml << param.fermact.xml;
    pop(xml);

    write(xml, "setupParam_1",  param.setupParam_1);

    pop(xml);
  }


  //! Propagator input
  void read(XMLReader& xml, const std::string& path, InlineBoxParams::NamedObject_t::Props_t& input)
  {
    XMLReader inputtop(xml, path);

    read(inputtop, "first_id", input.first_id);
    read(inputtop, "second_id", input.second_id);
    read(inputtop, "out_fname", input.out_fname);

  }

  //! Propagator output
  void write(XMLWriter& xml, const std::string& path, const InlineBoxParams::NamedObject_t::Props_t& input)
  {
    push(xml, path);

    write(xml, "first_id", input.first_id);
    write(xml, "second_id", input.second_id);
    write(xml, "out_fname", input.out_fname);

    pop(xml);
  }


  //! Propagator input
  void read(XMLReader& xml, const std::string& path, InlineBoxParams::NamedObject_t& input)
  {
    XMLReader inputtop(xml, path);

    read(inputtop, "gauge_id", input.gauge_id);
    read(inputtop, "prop_pairs", input.prop_pairs);
  }

  //! Propagator output
  void write(XMLWriter& xml, const std::string& path, const InlineBoxParams::NamedObject_t& input)
  {
    push(xml, path);

    write(xml, "gauge_id", input.gauge_id);
    write(xml, "prop_pairs", input.prop_pairs);

    pop(xml);
  }

  // Param stuff
  InlineBoxParams::InlineBoxParams()
  { 
    frequency = 0; 
  }

  InlineBoxParams::InlineBoxParams(XMLReader& xml_in, const std::string& path) 
  {
    try 
    {
      XMLReader paramtop(xml_in, path);

      if (paramtop.count("Frequency") == 1)
        read(paramtop, "Frequency", frequency);
      else
        frequency = 1;

      // Parameters for source construction
      read(paramtop, "Param", param);

      // Read in the output propagator/source configuration info
      read(paramtop, "NamedObject", named_obj);

      // Possible alternate XML file pattern
      if (paramtop.count("xml_file") != 0) 
      {
        read(paramtop, "xml_file", xml_file);
      }
    }
    catch(const std::string& e) 
    {
      QDPIO::cerr << __func__ << ": Caught Exception reading XML: " << e << std::endl;
      QDP_abort(1);
    }
  }


  void
    InlineBoxParams::write(XMLWriter& xml_out, const std::string& path) 
    {
      push(xml_out, path);

      Chroma::write(xml_out, "Param", param);
      Chroma::write(xml_out, "NamedObject", named_obj);
      QDP::write(xml_out, "xml_file", xml_file);

      pop(xml_out);
    }



  // Anonymous namespace
  namespace 
  {
    //! Useful structure holding sink props
    struct PropContainer_t
    {
      ForwardProp_t prop_header;
      std::string quark_propagator_id;
      Real Mass;

      multi1d<int> bc; 

      std::string source_type;
      std::string sink_type;
    };


    //! Useful structure holding sink props
    struct AllProps_t
    {
      PropContainer_t  prop_1;
      PropContainer_t  prop_2;
    };


    //! Read a prop
    void readProp(PropContainer_t& s, const std::string& id)
    {
      try
      {
        // Try a cast to see if it succeeds
        const LatticePropagator& foo = 
          TheNamedObjMap::Instance().getData<LatticePropagator>(id);

        // Snarf the data into a copy
        s.quark_propagator_id = id;

        // Snarf the prop info. This is will throw if the prop_id is not there
        XMLReader prop_file_xml, prop_record_xml;
        TheNamedObjMap::Instance().get(id).getFileXML(prop_file_xml);
        TheNamedObjMap::Instance().get(id).getRecordXML(prop_record_xml);

        // Read source and sink types
        {
          std::string xpath;
          read(prop_record_xml, "/SinkSmear", s.prop_header);

          read(prop_record_xml, "/SinkSmear/PropSource/Source/SourceType", s.source_type);
          read(prop_record_xml, "/SinkSmear/PropSink/Sink/SinkType", s.sink_type);
        }
      }
      catch( std::bad_cast ) 
      {
        QDPIO::cerr << InlineBoxEnv::name << ": caught dynamic cast error" 
          << std::endl;
        QDP_abort(1);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << InlineBoxEnv::name << ": error message: " << e 
          << std::endl;
        QDP_abort(1);
      }


      // Derived from input prop
      // Hunt around to find the mass
      QDPIO::cout << "Try action and mass" << std::endl;
      s.Mass = getMass(s.prop_header.prop_header.fermact);

      // Try to find boundary condition. If not present, assume dirichlet.
      s.bc.resize(Nd);
      s.bc = 0;

      try
      {
        s.bc = getFermActBoundary(s.prop_header.prop_header.fermact);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << InlineBoxEnv::name 
          << ": caught exception. No BC found in these headers. Will assume dirichlet: " << e 
          << std::endl;
      }

      QDPIO::cout << "FermAct = " << s.prop_header.prop_header.fermact.id << std::endl;
      QDPIO::cout << "Mass = " << s.Mass << std::endl;
    }


    //! Read all props
    void readAllProps(AllProps_t& s, 
        InlineBoxParams::NamedObject_t::Props_t prop_pair)
    {
      QDPIO::cout << "Attempt to parse forward propagator = " << prop_pair.first_id << std::endl;
      readProp(s.prop_1, prop_pair.first_id);
      QDPIO::cout << "Forward propagator successfully parsed" << std::endl;

      QDPIO::cout << "Attempt to parse forward propagator = " << prop_pair.second_id << std::endl;
      readProp(s.prop_2, prop_pair.second_id);
      QDPIO::cout << "Forward propagator successfully parsed" << std::endl;
    }

  } // namespace anonymous



  // Function call
  void 
    InlineBox::operator()(unsigned long update_no,
        XMLWriter& xml_out) 
    {
      // If xml file not empty, then use alternate
      if (params.xml_file != "")
      {
        std::string xml_file = makeXMLFileName(params.xml_file, update_no);

        push(xml_out, "boxdiag");
        write(xml_out, "update_no", update_no);
        write(xml_out, "xml_file", xml_file);
        pop(xml_out);

        XMLFileWriter xml(xml_file);
        func(update_no, xml);
      }
      else
      {
        func(update_no, xml_out);
      }
    }


  // Real work done here
  void 
    InlineBox::func(unsigned long update_no,
        XMLWriter& xml_out) 
    {
      START_CODE();

      StopWatch snoop;
      snoop.reset();
      snoop.start();

      // Test and grab a reference to the gauge field
      XMLBufferWriter gauge_xml;
      try
      {
        TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.gauge_id);
        TheNamedObjMap::Instance().get(params.named_obj.gauge_id).getRecordXML(gauge_xml);
      }
      catch( std::bad_cast ) 
      {
        QDPIO::cerr << InlineBoxEnv::name << ": caught dynamic cast error" 
          << std::endl;
        QDP_abort(1);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << InlineBoxEnv::name << ": std::map call failed: " << e 
          << std::endl;
        QDP_abort(1);
      }
      const multi1d<LatticeColorMatrix>& u = 
        TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.gauge_id);

      push(xml_out, "boxdiag");
      write(xml_out, "update_no", update_no);

      QDPIO::cout << " BOX: Box diagrams" << std::endl;
      QDPIO::cout << "     volume: " << Layout::lattSize()[0];
      for (int i=1; i<Nd; ++i) {
        QDPIO::cout << " x " << Layout::lattSize()[i];
      }
      QDPIO::cout << std::endl;

      proginfo(xml_out);    // Print out basic program info

      // Write out the input
      params.write(xml_out, "Input");

      // Write out the config info
      write(xml_out, "Config_info", gauge_xml);

      // Write output xml version
      push(xml_out, "Output_version");
      write(xml_out, "out_version", 1);
      pop(xml_out);


      // Keep an array of all the xml output buffers
      push(xml_out, "Measurements");

      multi2d<const LatticePropagator*> list_props(params.named_obj.prop_pairs.size(), 2);
      multi2d<int>                      list_tsrcs(params.named_obj.prop_pairs.size(), 2);
      multi1d<std::string>              list_outfnames(params.named_obj.prop_pairs.size());

      // Loop over the various fermion pairs and make list of them to pass
      for(int lpair=0; lpair < params.named_obj.prop_pairs.size(); ++lpair)
      {
        const InlineBoxParams::NamedObject_t::Props_t named_obj = params.named_obj.prop_pairs[lpair];

        push(xml_out, "elem");

        AllProps_t all_props;
        readAllProps(all_props, named_obj);

        if (all_props.prop_1.source_type != "WALL_SOURCE" || 
            all_props.prop_2.source_type != "WALL_SOURCE")
        {
          QDPIO::cerr << "Error!! source_type must be WALL_SOURCE" << std::endl;
          QDP_abort(1);
        }

        int j_decay = all_props.prop_1.prop_header.source_header.j_decay;
        int t1      = all_props.prop_1.prop_header.source_header.t_source;
        int t2      = all_props.prop_2.prop_header.source_header.t_source;

        if (all_props.prop_2.prop_header.source_header.j_decay != j_decay)
        {
          QDPIO::cerr << "Error!! j_decay must be the same for all propagators " << std::endl;
          QDP_abort(1);
        }
        if (all_props.prop_1.source_type != all_props.prop_2.source_type)
        {
          QDPIO::cerr << "Error!! source_type must be the same in a pair " << std::endl;
          QDP_abort(1);
        }
        if (all_props.prop_1.sink_type != all_props.prop_2.sink_type)
        {
          QDPIO::cerr << "Error!! source_type must be the same in a pair " << std::endl;
          QDP_abort(1);
        }

        int bc_spec = 0;
        bc_spec = all_props.prop_1.bc[j_decay] ;
        if (all_props.prop_2.bc[j_decay] != bc_spec)
        {
          QDPIO::cerr << "Error!! bc must be the same for all propagators " << std::endl;
          QDP_abort(1);
        }

        // Masses
        write(xml_out, "Mass_1", all_props.prop_1.Mass);
        write(xml_out, "Mass_2", all_props.prop_2.Mass);
        write(xml_out, "t1", t1);
        write(xml_out, "t2", t2);

        // Save prop input
        push(xml_out, "Forward_prop_headers");
        write(xml_out, "First_forward_prop", all_props.prop_1.prop_header);
        write(xml_out, "Second_forward_prop", all_props.prop_2.prop_header);
        pop(xml_out);

        push(xml_out, "SourceSinkType");
        {
          QDPIO::cout << "Source_type_1 = " << all_props.prop_1.source_type << std::endl;
          QDPIO::cout << "Sink_type_1 = " << all_props.prop_1.sink_type << std::endl;
          QDPIO::cout << "Source_type_2 = " << all_props.prop_2.source_type << std::endl;
          QDPIO::cout << "Sink_type_2 = " << all_props.prop_2.sink_type << std::endl;

          write(xml_out, "source_type_1", all_props.prop_1.source_type);
          write(xml_out, "sink_type_1", all_props.prop_1.sink_type);

          write(xml_out, "source_type_2", all_props.prop_2.source_type);
          write(xml_out, "sink_type_2", all_props.prop_2.sink_type);
        }
        pop(xml_out);


        // References for use later
        const LatticePropagator& prop_1 = 
          TheNamedObjMap::Instance().getData<LatticePropagator>(all_props.prop_1.quark_propagator_id);
        const LatticePropagator& prop_2 = 
          TheNamedObjMap::Instance().getData<LatticePropagator>(all_props.prop_2.quark_propagator_id);


        std::string src_type = "Wall";
        std::string snk_type;
        if (all_props.prop_1.sink_type == "POINT_SINK")
          snk_type = "Point";
        else
        {
          QDPIO::cerr << "Unsupported sink type = " << all_props.prop_1.sink_type << std::endl;
          QDP_abort(1);
        }

        std::string source_sink_type = src_type + "_" + snk_type;
        QDPIO::cout << "Source type = " << src_type << std::endl;
        QDPIO::cout << "Sink type = "   << snk_type << std::endl;

        list_props[lpair][0] = &prop_1;
        list_props[lpair][1] = &prop_2;
        list_tsrcs[lpair][0] = t1;
        list_tsrcs[lpair][1] = t2;

        list_outfnames[lpair] = named_obj.out_fname;

        pop(xml_out);  // array element
      } // for(lpair)

      // Calculations for Pions
      if (params.param.PionP) 
      {
        multi1d<LatticeColorMatrix> u;
        u = TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.gauge_id);

        box_pion(list_props, list_tsrcs, list_outfnames,
          params.param.T_ins, params.param.N_ins, params.param.rseed, 
          params.param.DiagAB, params.param.DiagC,
          params.param.invParam, params.param.invParam_1, params.param.fermact, params.param.setupParam_1, u, 
          xml_out);
      }

      pop(xml_out);  // Wilson_spectroscopy
      pop(xml_out);  // hadspec

      snoop.stop();
      QDPIO::cout << InlineBoxEnv::name << ": total time = "
        << snoop.getTimeInSeconds() 
        << " secs" << std::endl;

      QDPIO::cout << InlineBoxEnv::name << ": ran successfully" << std::endl;

      END_CODE();
    } 

};
