/*! \file
 * \brief Inline construction of wilson_flow_props
 *
 * Wilson flow propagators
 */

#include "handle.h"
#include "meas/inline/hadron/inline_wilson_flow_props_w.h"
#include "meas/inline/abs_inline_measurement_factory.h"
#include "util/info/unique_id.h"

#include "meas/inline/io/named_objmap.h"
#include "meas/glue/mesplq.h"

#include "actions/ferm/fermstates/ferm_createstate_factory_w.h"
#include "actions/ferm/fermstates/ferm_createstate_aggregate_w.h"

// Wilson Flow
#include "util/gauge/stout_utils.h"
#include "util/gauge/expmat.h"
#include "util/gauge/taproj.h"

namespace Chroma 
{ 
  //! Propagator input
  void read(XMLReader& xml, const std::string& path, InlineWilsonFlowPropsEnv::Params::NamedObject_t& input)
  {
    XMLReader inputtop(xml, path);

    read(inputtop, "gauge_id", input.gauge_id);
    read(inputtop, "prop_ids", input.prop_ids);
    read(inputtop, "flowed_gauge_id", input.flowed_gauge_id);
    read(inputtop, "flowed_prop_ids", input.flowed_prop_ids);
    
    if (inputtop.count("seqprop_ids") != 0)
      read(inputtop, "seqprop_ids", input.seqprop_ids);

    if (inputtop.count("flowed_seqprop_ids") != 0)
      read(inputtop, "flowed_seqprop_ids", input.flowed_seqprop_ids);

    if(input.prop_ids.size() != input.flowed_prop_ids.size())
    {
      QDPIO::cerr << InlineWilsonFlowPropsEnv::name << ": number of prop_ids != number of flowed_prop_ids." << std::endl;
      QDP_abort(1);
    }

    if(input.seqprop_ids.size() != input.flowed_seqprop_ids.size())
    {
      QDPIO::cerr << InlineWilsonFlowPropsEnv::name << ": number of seqprop_ids != number of flowed_seqprop_ids." << std::endl;
      QDP_abort(1);
    }

  }

  //! Propagator output
  void write(XMLWriter& xml, const std::string& path, const InlineWilsonFlowPropsEnv::Params::NamedObject_t& input)
  {
    push(xml, path);

    write(xml, "gauge_id", input.gauge_id);
    write(xml, "prop_ids", input.prop_ids);
    write(xml, "seqprop_ids", input.seqprop_ids);
    write(xml, "flowed_gauge_id", input.flowed_gauge_id);
    write(xml, "flowed_prop_ids", input.flowed_prop_ids);
    write(xml, "flowed_seqprop_ids", input.flowed_seqprop_ids);

    pop(xml);
  }


  //! Wilson Flow parameters
  void read(XMLReader& xml, const std::string& path, InlineWilsonFlowPropsEnv::Params::WilsonFlow_t& input)
  {
    XMLReader inputtop(xml, path);

    int version;
    read(inputtop, "version", version);

    input.cfs = CreateFermStateEnv::nullXMLGroup();

    switch (version)
    {
    case 1:
      read(inputtop, "wtime", input.wtime);
      read(inputtop, "weps",  input.weps);

      input.BkwdPropG5Format = "Unknown";
      if (inputtop.count("BkwdPropG5Format") != 0)
        read(inputtop, "BkwdPropG5Format",  input.BkwdPropG5Format);

      if (inputtop.count("FermState") != 0)
        input.cfs = readXMLGroup(inputtop, "FermState", "Name");
      break;

    default :
      QDPIO::cerr << InlineWilsonFlowPropsEnv::name << ": input parameter version "
      << version << " is not supported." << std::endl;
      QDP_abort(1);
    }
  }

  //! Wilson Flow parameters
  void write(XMLWriter& xml, const std::string& path, const InlineWilsonFlowPropsEnv::Params::WilsonFlow_t& input)
  {
    push(xml, path);

    int version = 1;
    write(xml, "version", version);
    write(xml, "wtime", input.wtime);
    write(xml, "weps",  input.weps);
    write(xml, "BkwdPropG5Format",  input.BkwdPropG5Format);
    xml << input.cfs.xml;

    pop(xml);
  }

  namespace InlineWilsonFlowPropsEnv 
  {
    namespace
    {
      AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
					      const std::string& path) 
      {
	return new InlineMeas(Params(xml_in, path));
      }

      //! Local registration flag
      bool registered = false;
    }

    const std::string name = "WILSON_FLOW_PROPS";

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
  success &= CreateFermStateEnv::registerAll();
	success &= TheInlineMeasurementFactory::Instance().registerObject(name, createMeasurement);
	registered = true;
      }
      return success;
    }


    // Param stuff
    Params::Params() { frequency = 0; }

    Params::Params(XMLReader& xml_in, const std::string& path) 
    {
      try 
      {
	XMLReader paramtop(xml_in, path);

	if (paramtop.count("Frequency") == 1)
	  read(paramtop, "Frequency", frequency);
	else
	  frequency = 1;

	// Parameters for source construction
	read(paramtop, "Param", wflow_params);

	// Read in the output propagator/source configuration info
	read(paramtop, "NamedObject", named_obj);
      }
      catch(const std::string& e) 
      {
	QDPIO::cerr << __func__ << ": Caught Exception reading XML: " << e << std::endl;
	QDP_abort(1);
      }
    }


    void
    Params::writeXML(XMLWriter& xml_out, const std::string& path) 
    {
      push(xml_out, path);
    
      // Parameters for source construction
      write(xml_out, "Param", wflow_params);

      // Write out the output propagator/source configuration info
      write(xml_out, "NamedObject", named_obj);

      pop(xml_out);
    }


    void 
    InlineMeas::operator()(unsigned long update_no,
			   XMLWriter& xml_out) 
    {
      START_CODE();

      QDPIO::cout << name << ": Wilson flow propagators" << std::endl;

      StopWatch snoop;
      snoop.reset();
      snoop.start();

      // Test and grab a reference to the gauge field
      XMLBufferWriter gauge_xml;
      try
      {
	TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.gauge_id);
	TheNamedObjMap::Instance().get(params.named_obj.gauge_id).getRecordXML(gauge_xml);
      }
      catch( std::bad_cast ) 
      {
	QDPIO::cerr << name << ": caught dynamic cast error" 
		    << std::endl;
	QDP_abort(1);
      }
      catch (const std::string& e) 
      {
	QDPIO::cerr << name << ": std::map call failed: " << e 
		    << std::endl;
	QDP_abort(1);
      }
      const multi1d<LatticeColorMatrix>& u = 
	TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.gauge_id);

      push(xml_out, "wilson_flow_props");
      write(xml_out, "update_no", update_no);

      // Write out the input
      params.writeXML(xml_out, "Input");

      // Write out the config header
      write(xml_out, "Config_info", gauge_xml);

      // Calculate some gauge invariant observables just for info.
      int N_props    = params.named_obj.prop_ids.size();
      int N_seqprops = params.named_obj.seqprop_ids.size();
      int N_props_tot = N_props + N_seqprops;

      //
      // Read the quark propagator and extract headers
      //
      multi1d<XMLReader> prop_file_xml(N_props_tot);
      multi1d<XMLReader> prop_record_xml(N_props_tot);
      multi1d<LatticePropagator> props(N_props_tot);


      QDPIO::cout << "Attempt to read propagator" << std::endl;
      for(int i_prop=0; i_prop < N_props_tot; ++i_prop)
      {
        //first N_props are props, later are sequential props
        bool SEQ_PROP = false;
        int i_seqprop = -1;
        if(i_prop >= N_props){
          SEQ_PROP = true;
          i_seqprop = i_prop-N_props;
        }

        try
        {
          // Grab a copy of the propagator
          if(SEQ_PROP){
            props[i_prop] = 
              TheNamedObjMap::Instance().getData<LatticePropagator>(params.named_obj.seqprop_ids[i_seqprop]);

            TheNamedObjMap::Instance().get(params.named_obj.seqprop_ids[i_seqprop]).getFileXML(prop_file_xml[i_prop]);
            TheNamedObjMap::Instance().get(params.named_obj.seqprop_ids[i_seqprop]).getRecordXML(prop_record_xml[i_prop]);
          }
          else{
            props[i_prop] = 
              TheNamedObjMap::Instance().getData<LatticePropagator>(params.named_obj.prop_ids[i_prop]);

            TheNamedObjMap::Instance().get(params.named_obj.prop_ids[i_prop]).getFileXML(prop_file_xml[i_prop]);
            TheNamedObjMap::Instance().get(params.named_obj.prop_ids[i_prop]).getRecordXML(prop_record_xml[i_prop]);
          }

          // Write out the propagator header
          write(xml_out, "Prop_file_info", prop_file_xml[i_prop]);
          write(xml_out, "Prop_record_info", prop_record_xml[i_prop]);
        }    
        catch (std::bad_cast)
        {
          QDPIO::cerr << name << ": caught dynamic cast error" 
            << std::endl;
          QDP_abort(1);
        }
        catch (const std::string& e) 
        {
          QDPIO::cerr << name << ": error extracting prop_header: " << e << std::endl;
          QDP_abort(1);
        }
      } // for(i_prop)


      // First N_props elements in props are regular propagators, and the later N_seqprop are sequential propagators
      // The sequential propagators are multiplied by g5 due to the construction of the sequential source
      for(int i_prop=N_props; i_prop < N_props_tot; ++i_prop)
      {
        if(params.wflow_params.BkwdPropG5Format == "G5_B_G5")
        {
          props[i_prop] = Gamma(15) * props[i_prop] * Gamma(15);
        }
        else
        {
          QDPIO::cerr << name << ": Unsupported BkwdPropG5Format " << params.wflow_params.BkwdPropG5Format << std::endl;
          QDP_abort(1);
        }

        // In order to flow the side contracted with the inserted operator, perform gamma_5 hermiticity
        props[i_prop] = Gamma(15) * adj(props[i_prop]) * Gamma(15);
      } // for(i_prop)

      //-------------------------------
      // Run Wilson Flow
      //-------------------------------
      multi1d<LatticeColorMatrix> u_smr = u;
      func(u_smr, params.wflow_params.wtime, params.wflow_params.weps, params.wflow_params.cfs, props);

      // Back to original sequential props
      for(int i_prop=N_props; i_prop < N_props_tot; ++i_prop)
      {
        if(params.wflow_params.BkwdPropG5Format == "G5_B_G5")
        {
          props[i_prop] = Gamma(15) * props[i_prop] * Gamma(15);
        }
        else
        {
          QDPIO::cerr << name << ": Unsupported BkwdPropG5Format " << params.wflow_params.BkwdPropG5Format << std::endl;
          QDP_abort(1);
        }

        // Back to original of gamma_5 hermiticity
        props[i_prop] = Gamma(15) * adj(props[i_prop]) * Gamma(15);
      } // for(i_prop)

      // Save the propagator
      try
      {
        for(int i_prop=0; i_prop < N_props_tot; ++i_prop)
        {
          //first N_props are props, later are sequential props
          bool SEQ_PROP = false;
          int i_seqprop = -1;
          if(i_prop >= N_props){
            SEQ_PROP = true;
            i_seqprop = i_prop-N_props;
          }

          XMLBufferWriter file_xml;
          push(file_xml, "wilson_flow_props");
          write(file_xml, "id", uniqueId());  // NOTE: new ID form
          pop(file_xml);
  
          XMLBufferWriter record_xml;
  
          if (prop_record_xml[i_prop].count("/Propagator") != 0)
          {
            Propagator_t  orig_header;
            read(prop_record_xml[i_prop], "/Propagator", orig_header);
  
            write(record_xml, "Propagator", orig_header);  
          } 
          if (prop_record_xml[i_prop].count("/SequentialProp") != 0)
          {
            SequentialProp_t  orig_header;
            read(prop_record_xml[i_prop], "/SequentialProp", orig_header);
  
            write(record_xml, "SequentialProp", orig_header);  
          } 
          if (prop_record_xml[i_prop].count("/Propagator") == 0 && prop_record_xml[i_prop].count("/SequentialProp") == 0) 
          {
            throw std::string("No appropriate header found");
          }
  
          // Write the flowed prop xml info
          if(SEQ_PROP)
          {
            TheNamedObjMap::Instance().create<LatticePropagator>(params.named_obj.flowed_seqprop_ids[i_seqprop]);
            TheNamedObjMap::Instance().getData<LatticePropagator>(params.named_obj.flowed_seqprop_ids[i_seqprop]) 
              = props[i_prop];
            TheNamedObjMap::Instance().get(params.named_obj.flowed_seqprop_ids[i_seqprop]).setFileXML(file_xml);
            TheNamedObjMap::Instance().get(params.named_obj.flowed_seqprop_ids[i_seqprop]).setRecordXML(record_xml);
          }
          else
          {
            TheNamedObjMap::Instance().create<LatticePropagator>(params.named_obj.flowed_prop_ids[i_prop]);
            TheNamedObjMap::Instance().getData<LatticePropagator>(params.named_obj.flowed_prop_ids[i_prop]) 
              = props[i_prop];
            TheNamedObjMap::Instance().get(params.named_obj.flowed_prop_ids[i_prop]).setFileXML(file_xml);
            TheNamedObjMap::Instance().get(params.named_obj.flowed_prop_ids[i_prop]).setRecordXML(record_xml);
          }
        } // for(i_prop)

        // Block:save gauge
        {
          XMLBufferWriter file_xml, record_xml;
          push(file_xml, "gauge");
          write(file_xml, "id", int(0));
          pop(file_xml);
          record_xml << gauge_xml;

          // Store the gauge field
          TheNamedObjMap::Instance().create< multi1d<LatticeColorMatrix> >(params.named_obj.flowed_gauge_id);
          TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.flowed_gauge_id) = u_smr;
          TheNamedObjMap::Instance().get(params.named_obj.flowed_gauge_id).setFileXML(file_xml);
          TheNamedObjMap::Instance().get(params.named_obj.flowed_gauge_id).setRecordXML(record_xml);
        } // Block:save gauge

        QDPIO::cout << "Wilson flowed gauge and propagators successfully updated" << std::endl;
      }
      catch (std::bad_cast)
      {
        QDPIO::cerr << name << ": dynamic cast error" 
          << std::endl;
        QDP_abort(1);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << name << ": error message: " << e << std::endl;
        QDP_abort(1);
      }

      pop(xml_out);  // wilson_flow_props


      snoop.stop();
      QDPIO::cout << name << ": total time = "
        << snoop.getTimeInSeconds() 
        << " secs" << std::endl;

      QDPIO::cout << name << ": ran successfully" << std::endl;
    } 

  
    //###################################################################################//
    // D_mu D_mu for Wilson Flow Smearing                                                //
    //###################################################################################//
    // Luscher's arXiv:1302.5246, Eq (2.5) defines D_muD_mu operators used in Wilson Flow
    // Here D_mu Psi(x) = (U_mu(x) Psi(x+mu) - U_mu^dag(x-mu) Psi(x-mu))/2
    LatticePropagator DmuDmu(multi1d<LatticeColorMatrix> & u, LatticePropagator &P)
    {
      LatticePropagator P_ret = zero;
      for(int mu = 0; mu < Nd; ++mu)
      {
        // This is inefficient because one can define the finite second derivative directly.
        LatticePropagator P_tmp = P;
        for(int i_dmu = 0; i_dmu < 2; ++i_dmu)
        {
          LatticePropagator tmp1  = u[mu] * shift(P_tmp, FORWARD, mu);
          tmp1                   -= shift(adj(u[mu])*P_tmp, BACKWARD, mu);
          P_tmp = Real(0.5) * tmp1;
        } // for(i_dmu)
        P_ret += P_tmp;
      }// for(mu)
  
      return P_ret;
    } // LatticePropagator DmuDmu()
  
  
  
    //###################################################################################//
    // Wilson Flow Smearing (for backward and forward props)                             //
    //###################################################################################//
    // Following Luscher's arXiv:1302.5246, Appendix D.
    // Link smearing is taken from lib/meas/glue/wilson_flow_w.cc
    void InlineMeas::func(multi1d<LatticeColorMatrix> & u, Real wtime, Real weps, GroupXML_t cfs,
        multi1d<LatticePropagator> &props)
    {
      int nstep = toInt(ceil(wtime/weps));
      Real rho = weps;

      if(toBool(nstep*weps != wtime))
      {
        QDPIO::cerr << name << ": wtime should be multiple of weps, but wtime=" << wtime << ", weps=" << weps << std::endl;
        QDP_abort(1);
      }
  
      QDPIO::cout << "wflow nstep  = " << nstep << std::endl;
      for(int i_wsteps=0; i_wsteps<nstep; ++i_wsteps)
      {
        int mu, dir;
  
        //---------------------------------------------
        // Update Guage Field
        //---------------------------------------------
        // inefficient memory usage; need further reduction
        multi1d<LatticeColorMatrix> W0(Nd);
        multi1d<LatticeColorMatrix> W1(Nd);
        multi1d<LatticeColorMatrix> W2(Nd);
  
        multi1d<LatticeColorMatrix> dest(Nd);
        multi1d<LatticeColorMatrix> next(Nd);
  
        multi1d<bool> smear_in_this_dirP(4) ;
        multi2d<Real> rho_a(4,4) ;
        multi2d<Real> rho_b1(4,4) ;
        multi2d<Real> rho_b2(4,4) ;
        multi2d<Real> rho_c(4,4) ;
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          smear_in_this_dirP(mu) = true ;
          for (dir = 0; dir <= Nd-1; dir++)
          {
            rho_a[mu][dir] = rho * 0.25 ;
  
            rho_b1[mu][dir] = rho * 8.0/9.0 ;
            rho_b2[mu][dir] = rho * 17.0/36.0 ;
  
            rho_c[mu][dir] = rho * 3.0/4.0 ;
          }
        }
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          W0[mu] = u[mu];
        }
  
        Stouting::smear_links(u, dest,smear_in_this_dirP, rho_a);
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          W1[mu] = dest[mu];
        }
  
        LatticeColorMatrix  Q, QQ, C ;
        LatticeColorMatrix  Q2, QQ2  ;
  
        multi1d<LatticeColorMatrix> Q0(Nd);
        multi1d<LatticeColorMatrix> Q1(Nd);
  
  
        multi1d<LatticeComplex> f;   // routine will resize these
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          Stouting::getQsandCs(dest, Q1[mu],QQ,C,mu,smear_in_this_dirP,rho_b1) ;
          Stouting::getQsandCs(u   , Q0[mu],QQ,C,mu,smear_in_this_dirP,rho_b2) ;
  
          Q = Q1[mu] - Q0[mu] ;
          QQ = Q * Q ;
          Stouting::getFs(Q,QQ,f);   // This routine computes the f-s
  
          // Assemble the stout links exp(iQ)U_{mu}
          next[mu]=(f[0] + f[1]*Q + f[2]*QQ)*dest[mu];
  
        }
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          u[mu]    =  next[mu] ;
          dest[mu] =  next[mu] ;
        }
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          W2[mu] = dest[mu];
        }
  
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          Stouting::getQsandCs(dest, Q2,QQ,C,mu,smear_in_this_dirP,rho_c) ;
  
          Q = Q2 - Q1[mu] + Q0[mu] ;
          QQ = Q * Q ;
          Stouting::getFs(Q,QQ,f);   // This routine computes the f-s
  
          // Assemble the stout links exp(iQ)U_{mu}
          next[mu]=(f[0] + f[1]*Q + f[2]*QQ)*dest[mu];
  
        }
  
        for (mu = 0; mu <= Nd-1; mu++)
        {
          u[mu]    =  next[mu] ;
        }
  
        //---------------------------------------------
        // Update Forward and Backward props
        //---------------------------------------------

        // block: Modify gauge field for fermion boundary condition
        {
          //QDPIO::cout << "cfs=XX" << cfs.xml << "XX" << std::endl;
          std::istringstream  xml_s(cfs.xml);
          XMLReader  fermtop(xml_s);

          Handle<CreateFermState< LatticeFermion, multi1d<LatticeColorMatrix>, multi1d<LatticeColorMatrix> > >
            m_cfs(TheCreateFermStateFactory::Instance().createObject(cfs.id,
                  fermtop,
                  cfs.path));

          // Pull the u fields back out from the state since they might have been munged with fermBC's
          Handle<FermState< LatticeFermion, multi1d<LatticeColorMatrix>, multi1d<LatticeColorMatrix> > >
            state0((*m_cfs)(W0));
          W0 = state0->getLinks();

          Handle<FermState< LatticeFermion, multi1d<LatticeColorMatrix>, multi1d<LatticeColorMatrix> > >
            state1((*m_cfs)(W1));
          W1 = state1->getLinks();

          Handle<FermState< LatticeFermion, multi1d<LatticeColorMatrix>, multi1d<LatticeColorMatrix> > >
            state2((*m_cfs)(W2));
          W2 = state2->getLinks();
        }// block: modify gauge field

        Real c1_4 = rho*1.0/4.0;
        Real c8_9 = rho*8.0/9.0;
        Real c2_9 = rho*2.0/9.0;
        Real c3_4 = rho*3.0/4.0;
  
        for(int i_prop = 0; i_prop < props.size(); ++i_prop)
        {
          LatticePropagator DmuDmuP = DmuDmu(W0, props[i_prop]);
          LatticePropagator phi1 = props[i_prop] + c1_4 * DmuDmuP;
          LatticePropagator phi2 = props[i_prop] + c8_9 * DmuDmu(W1, phi1) - c2_9 * DmuDmuP;
          props[i_prop] = phi1 + c3_4 * DmuDmu(W2, phi2);
        }
  
      } //for(int i_wsteps=0; i_wsteps<nstep, ++i_wsteps)
    } // RunWilsonFlow()
  }

}
