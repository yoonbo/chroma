// -*- C++ -*-
/*! \file
 * \brief Inline box diagram calculations
 *
 * Box diagram calculations
 */

#ifndef __inline_box_h__
#define __inline_box_h__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"
#include "util/ft/sftmom.h"
#include "io/xml_group_reader.h"
#include "meas/inline/io/named_objmap.h"

namespace Chroma 
{ 
  /*! \ingroup inlinehadron */
  namespace InlineBoxEnv 
  {
    extern const std::string name;
    bool registerAll();
  }

  //! Parameter structure
  /*! \ingroup inlinehadron */
  struct InlineBoxParams 
  {
    InlineBoxParams();
    InlineBoxParams(XMLReader& xml_in, const std::string& path);
    void write(XMLWriter& xml_out, const std::string& path);

    unsigned long frequency;

    struct Param_t
    {
      bool PionP;    // Pion
      bool KaonP;    // Kaon
      bool NeutronP; // Nucleon

      bool DiagAB;  // Diagram A and B
      bool DiagC;   // Diagram C

      int T_ins;    // timeslice of X-current insertion
      int N_ins;    // number of X-current insertions
      int rseed;    // seed for the random number generator

      GroupXML_t  invParam;
      GroupXML_t  invParam_1;
      GroupXML_t  fermact;
      bool        setupParam_1;
//      int mom2_max;            // (mom)^2 <= mom2_max. mom2_max=7 in szin.
    } param;

    struct NamedObject_t
    {
      std::string  gauge_id;

      struct Props_t
      {
        std::string  first_id;
        std::string  second_id;
        std::string  out_fname; // output file name of this combination of propagators
      };

      multi1d<Props_t> prop_pairs;
    } named_obj;

    std::string xml_file;  // Alternate XML file pattern
  };


  //! Inline measurement of hadron spectrum
  /*! \ingroup inlinehadron */
  class InlineBox : public AbsInlineMeasurement 
  {
    public:
      ~InlineBox() {}
      InlineBox(const InlineBoxParams& p) : params(p) {}
      InlineBox(const InlineBox& p) : params(p.params) {}

      unsigned long getFrequency(void) const {return params.frequency;}

      //! Do the measurement
      void operator()(const unsigned long update_no,
          XMLWriter& xml_out); 

    protected:
      //! Do the measurement
      void func(const unsigned long update_no,
          XMLWriter& xml_out); 

    private:
      InlineBoxParams params;
  };

};

#endif
