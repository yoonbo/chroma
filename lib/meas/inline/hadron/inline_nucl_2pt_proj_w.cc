/*! \file
 * \brief Inline nucleon two-point function with gamma projection 
 *
 * Nucleon two-point function with gamma projection
 */

#include "meas/inline/hadron/inline_nucl_2pt_proj_w.h"
#include "meas/inline/abs_inline_measurement_factory.h"
#include "meas/glue/mesplq.h"
#include "util/ft/sftmom.h"
#include "util/info/proginfo.h"
#include "io/param_io.h"
#include "io/qprop_io.h"
#include "meas/hadron/nucl_2pt_proj_w.h"
#include "meas/inline/make_xml_file.h"
#include "meas/inline/io/named_objmap.h"
#include "meas/smear/no_quark_displacement.h"

namespace Chroma 
{ 
  namespace InlineNuclTwoptProjEnv 
  { 
    namespace
    {
      AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
          const std::string& path) 
      {
        return new InlineNuclTwoptProj(InlineNuclTwoptProjParams(xml_in, path));
      }

      //! Local registration flag
      bool registered = false;
    }

    const std::string name = "NUCLEON_2PT_PROJ";

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
        success &= TheInlineMeasurementFactory::Instance().registerObject(name, createMeasurement);
        registered = true;
      }
      return success;
    }
  }



  //! Reader for parameters
  void read(XMLReader& xml, const std::string& path, InlineNuclTwoptProjParams::Param_t& param)
  {
    XMLReader paramtop(xml, path);

    param.axial_proj_only = true;
    param.cEDM = false;

    read(paramtop, "version", param.version);

    switch (param.version) 
    {
      case 1:
        param.cEDM = false;
        break;

      case 2:
        read(paramtop, "cEDM", param.cEDM);
        break;

      case 3:
        read(paramtop, "axial_proj_only", param.axial_proj_only);
        break;

      default:
        QDPIO::cerr << "Input parameter version " << param.version << " unsupported." << std::endl;
        QDP_abort(1);
    }

    read(paramtop, "mom2_max", param.mom2_max);
    read(paramtop, "avg_equiv_mom", param.avg_equiv_mom);

    if(paramtop.count("Multi_Srcs")==1)
    {
      param.multi_srcsP = true;
      read(paramtop, "Multi_Srcs", param.multi_srcs);
    }
    else
      param.multi_srcsP = false;
  }


  //! Writer for parameters
  void write(XMLWriter& xml, const std::string& path, const InlineNuclTwoptProjParams::Param_t& param)
  {
    push(xml, path);

    write(xml, "version", param.version);

    write(xml, "axial_proj_only", param.axial_proj_only);
    write(xml, "mom2_max", param.mom2_max);
    write(xml, "avg_equiv_mom", param.avg_equiv_mom);

    if(param.version==2)
      write(xml, "cEDM", param.cEDM);

    if(param.multi_srcsP)
      write(xml, "Multi_Srcs", param.multi_srcs);

    pop(xml);
  }


  //! Propagator input
  void read(XMLReader& xml, const std::string& path, InlineNuclTwoptProjParams::NamedObject_t::Props_t& input)
  {
    XMLReader inputtop(xml, path);

    read(inputtop, "first_id", input.first_id);
    read(inputtop, "second_id", input.second_id);
  }

  //! Propagator output
  void write(XMLWriter& xml, const std::string& path, const InlineNuclTwoptProjParams::NamedObject_t::Props_t& input)
  {
    push(xml, path);

    write(xml, "first_id", input.first_id);
    write(xml, "second_id", input.second_id);

    pop(xml);
  }


  //! Propagator input
  void read(XMLReader& xml, const std::string& path, InlineNuclTwoptProjParams::NamedObject_t& input)
  {
    XMLReader inputtop(xml, path);

    read(inputtop, "gauge_id", input.gauge_id);
    read(inputtop, "sink_pairs", input.sink_pairs);
  }

  //! Propagator output
  void write(XMLWriter& xml, const std::string& path, const InlineNuclTwoptProjParams::NamedObject_t& input)
  {
    push(xml, path);

    write(xml, "gauge_id", input.gauge_id);
    write(xml, "sink_pairs", input.sink_pairs);

    pop(xml);
  }

  // Param stuff
  InlineNuclTwoptProjParams::InlineNuclTwoptProjParams()
  { 
    frequency = 0; 
  }

  InlineNuclTwoptProjParams::InlineNuclTwoptProjParams(XMLReader& xml_in, const std::string& path) 
  {
    try 
    {
      XMLReader paramtop(xml_in, path);

      if (paramtop.count("Frequency") == 1)
        read(paramtop, "Frequency", frequency);
      else
        frequency = 1;

      // Parameters for source construction
      read(paramtop, "Param", param);

      // Read in the output propagator/source configuration info
      read(paramtop, "NamedObject", named_obj);

      // Possible alternate XML file pattern
      if (paramtop.count("xml_file") != 0) 
      {
        read(paramtop, "xml_file", xml_file);
      }
    }
    catch(const std::string& e) 
    {
      QDPIO::cerr << __func__ << ": Caught Exception reading XML: " << e << std::endl;
      QDP_abort(1);
    }
  }


  void
    InlineNuclTwoptProjParams::write(XMLWriter& xml_out, const std::string& path) 
    {
      push(xml_out, path);

      Chroma::write(xml_out, "Param", param);
      Chroma::write(xml_out, "NamedObject", named_obj);
      QDP::write(xml_out, "xml_file", xml_file);

      pop(xml_out);
    }



  // Anonymous namespace
  namespace 
  {
    //! Useful structure holding sink props
    struct SinkPropContainer_t
    {
      ForwardProp_t prop_header;
      std::string quark_propagator_id;
      Real Mass;

      multi1d<int> bc; 

      std::string source_type;
      std::string source_disp_type;
      std::string sink_type;
      std::string sink_disp_type;
    };


    //! Useful structure holding sink props
    struct AllSinkProps_t
    {
      SinkPropContainer_t  sink_prop_1;
      SinkPropContainer_t  sink_prop_2;
    };


    //! Read a sink prop
    void readSinkProp(SinkPropContainer_t& s, const std::string& id)
    {
      try
      {
        // Try a cast to see if it succeeds
        const LatticePropagator& foo = 
          TheNamedObjMap::Instance().getData<LatticePropagator>(id);

        // Snarf the data into a copy
        s.quark_propagator_id = id;

        // Snarf the prop info. This is will throw if the prop_id is not there
        XMLReader prop_file_xml, prop_record_xml;
        TheNamedObjMap::Instance().get(id).getFileXML(prop_file_xml);
        TheNamedObjMap::Instance().get(id).getRecordXML(prop_record_xml);

        // Try to invert this record XML into a ChromaProp struct
        // Also pull out the id of this source
        {
          std::string xpath;
          read(prop_record_xml, "/SinkSmear", s.prop_header);

          read(prop_record_xml, "/SinkSmear/PropSource/Source/SourceType", s.source_type);
          xpath = "/SinkSmear/PropSource/Source/Displacement/DisplacementType";
          if (prop_record_xml.count(xpath) != 0)
            read(prop_record_xml, xpath, s.source_disp_type);
          else
            s.source_disp_type = NoQuarkDisplacementEnv::getName();

          read(prop_record_xml, "/SinkSmear/PropSink/Sink/SinkType", s.sink_type);
          xpath = "/SinkSmear/PropSink/Sink/Displacement/DisplacementType";
          if (prop_record_xml.count(xpath) != 0)
            read(prop_record_xml, xpath, s.sink_disp_type);
          else
            s.sink_disp_type = NoQuarkDisplacementEnv::getName();
        }
      }
      catch( std::bad_cast ) 
      {
        QDPIO::cerr << InlineNuclTwoptProjEnv::name << ": caught dynamic cast error" 
          << std::endl;
        QDP_abort(1);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << InlineNuclTwoptProjEnv::name << ": error message: " << e 
          << std::endl;
        QDP_abort(1);
      }


      // Derived from input prop
      // Hunt around to find the mass
      // NOTE: this may be problematic in the future if actions are used with no
      // clear def. of a Mass
      QDPIO::cout << "Try action and mass" << std::endl;
      s.Mass = getMass(s.prop_header.prop_header.fermact);

      // Only baryons care about boundaries
      // Try to find them. If not present, assume dirichlet.
      s.bc.resize(Nd);
      s.bc = 0;

      try
      {
        s.bc = getFermActBoundary(s.prop_header.prop_header.fermact);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << InlineNuclTwoptProjEnv::name 
          << ": caught exception. No BC found in these headers. Will assume dirichlet: " << e 
          << std::endl;
      }

      QDPIO::cout << "FermAct = " << s.prop_header.prop_header.fermact.id << std::endl;
      QDPIO::cout << "Mass = " << s.Mass << std::endl;
    }


    //! Read all sinks
    void readAllSinks(AllSinkProps_t& s, 
        InlineNuclTwoptProjParams::NamedObject_t::Props_t sink_pair)
    {
      QDPIO::cout << "Attempt to parse forward propagator = " << sink_pair.first_id << std::endl;
      readSinkProp(s.sink_prop_1, sink_pair.first_id);
      QDPIO::cout << "Forward propagator successfully parsed" << std::endl;

      QDPIO::cout << "Attempt to parse forward propagator = " << sink_pair.second_id << std::endl;
      readSinkProp(s.sink_prop_2, sink_pair.second_id);
      QDPIO::cout << "Forward propagator successfully parsed" << std::endl;
    }

  } // namespace anonymous



  // Function call
  void 
    InlineNuclTwoptProj::operator()(unsigned long update_no,
        XMLWriter& xml_out) 
    {
      // If xml file not empty, then use alternate
      if (params.xml_file != "")
      {
        std::string xml_file = makeXMLFileName(params.xml_file, update_no);

        push(xml_out, "nucl_2pt_proj");
        write(xml_out, "update_no", update_no);
        write(xml_out, "xml_file", xml_file);
        pop(xml_out);

        XMLFileWriter xml(xml_file);
        func(update_no, xml);
      }
      else
      {
        func(update_no, xml_out);
      }
    }


  // Real work done here
  void 
    InlineNuclTwoptProj::func(unsigned long update_no,
        XMLWriter& xml_out) 
    {
      START_CODE();

      StopWatch snoop;
      snoop.reset();
      snoop.start();

      // Test and grab a reference to the gauge field
      XMLBufferWriter gauge_xml;
      try
      {
        TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.gauge_id);
        TheNamedObjMap::Instance().get(params.named_obj.gauge_id).getRecordXML(gauge_xml);
      }
      catch( std::bad_cast ) 
      {
        QDPIO::cerr << InlineNuclTwoptProjEnv::name << ": caught dynamic cast error" 
          << std::endl;
        QDP_abort(1);
      }
      catch (const std::string& e) 
      {
        QDPIO::cerr << InlineNuclTwoptProjEnv::name << ": map call failed: " << e 
          << std::endl;
        QDP_abort(1);
      }
      const multi1d<LatticeColorMatrix>& u = 
        TheNamedObjMap::Instance().getData< multi1d<LatticeColorMatrix> >(params.named_obj.gauge_id);

      push(xml_out, "nucl_2pt_proj");
      write(xml_out, "update_no", update_no);

      QDPIO::cout << " NUCLEON_2PT_PROJ: Nucleon two-point function with projections in Wilson-like fermions" << std::endl;
      QDPIO::cout << std::endl << "     Gauge group: SU(" << Nc << ")" << std::endl;
      QDPIO::cout << "     volume: " << Layout::lattSize()[0];
      for (int i=1; i<Nd; ++i) {
        QDPIO::cout << " x " << Layout::lattSize()[i];
      }
      QDPIO::cout << std::endl;

      proginfo(xml_out);    // Print out basic program info

      // Write out the input
      params.write(xml_out, "Input");

      // Write out the config info
      write(xml_out, "Config_info", gauge_xml);

      push(xml_out, "Output_version");
      if(params.param.axial_proj_only == true)
        write(xml_out, "out_version", 15);
      else
        write(xml_out, "out_version", 20);
      pop(xml_out);


      // First calculate some gauge invariant observables just for info.
      MesPlq(xml_out, "Observables", u);

      // Keep an array of all the xml output buffers
      push(xml_out, "Wilson_nucl_2pt_proj_measurements");

      // Now loop over the various fermion pairs
      for(int lpair=0; lpair < params.named_obj.sink_pairs.size(); ++lpair)
      {
        const InlineNuclTwoptProjParams::NamedObject_t::Props_t named_obj = params.named_obj.sink_pairs[lpair];

        push(xml_out, "elem");

        AllSinkProps_t all_sinks;
        readAllSinks(all_sinks, named_obj);

        // Derived from input prop
        multi1d<int> t_srce
          = all_sinks.sink_prop_1.prop_header.source_header.getTSrce();
        int j_decay = all_sinks.sink_prop_1.prop_header.source_header.j_decay;
        int t0      = all_sinks.sink_prop_1.prop_header.source_header.t_source;

        // Sanity checks
        {
          if (all_sinks.sink_prop_2.prop_header.source_header.j_decay != j_decay)
          {
            QDPIO::cerr << "Error!! j_decay must be the same for all propagators " << std::endl;
            QDP_abort(1);
          }
          if (all_sinks.sink_prop_2.prop_header.source_header.t_source != 
              all_sinks.sink_prop_1.prop_header.source_header.t_source)
          {
            QDPIO::cerr << "Error!! t_source must be the same for all propagators " << std::endl;
            QDP_abort(1);
          }
          if (all_sinks.sink_prop_1.source_type != all_sinks.sink_prop_2.source_type)
          {
            QDPIO::cerr << "Error!! source_type must be the same in a pair " << std::endl;
            QDP_abort(1);
          }
          if (all_sinks.sink_prop_1.sink_type != all_sinks.sink_prop_2.sink_type)
          {
            QDPIO::cerr << "Error!! source_type must be the same in a pair " << std::endl;
            QDP_abort(1);
          }
        }

        // bc 
        int bc_spec = 0;
        bc_spec = all_sinks.sink_prop_1.bc[j_decay] ;
        if (all_sinks.sink_prop_2.bc[j_decay] != bc_spec)
        {
          QDPIO::cerr << "Error!! bc must be the same for all propagators " << std::endl;
          QDP_abort(1);
        }

        SftMom *p_phases;  // Pointer for phases

        if(params.param.multi_srcsP)
        {
          multi1d<SftMomSrcPos_t> s = params.param.multi_srcs;
          QDPIO::cout << "Multiple source positions applied in Fourier Transf:" << std::endl;
          for(int i=0; i<s.size(); ++i)
          {
            QDPIO::cout << "  src=( ";
            for(int j=0; j<s[i].src.size(); ++j)
              QDPIO::cout << s[i].src[j] << " ";

            QDPIO::cout << ")  for  t=[" << s[i].tA << ", " << s[i].tB << "]" << std::endl;
          }
        } // End: if(params.param.multi_srcsP)

        if(params.param.multi_srcsP)
          p_phases = new SftMom(params.param.mom2_max, params.param.multi_srcs,
              params.param.avg_equiv_mom, j_decay);
        else
          p_phases = new SftMom(params.param.mom2_max, t_srce,
              params.param.avg_equiv_mom, j_decay);

        SftMom& phases = *p_phases; // Avoid pointer operand (->)

        // Keep a copy of the phases with NO momenta
        SftMom phases_nomom(0, true, j_decay);

        // Masses
        write(xml_out, "Mass_1", all_sinks.sink_prop_1.Mass);
        write(xml_out, "Mass_2", all_sinks.sink_prop_2.Mass);
        write(xml_out, "t0", t0);

        // Save prop input
        push(xml_out, "Forward_prop_headers");
        write(xml_out, "First_forward_prop", all_sinks.sink_prop_1.prop_header);
        write(xml_out, "Second_forward_prop", all_sinks.sink_prop_2.prop_header);
        pop(xml_out);

        // Sanity check - write out the norm2 of the forward prop in the j_decay direction
        // Use this for any possible verification
        push(xml_out, "Forward_prop_correlator");
        {
          const LatticePropagator& sink_prop_1 = 
            TheNamedObjMap::Instance().getData<LatticePropagator>(all_sinks.sink_prop_1.quark_propagator_id);
          const LatticePropagator& sink_prop_2 = 
            TheNamedObjMap::Instance().getData<LatticePropagator>(all_sinks.sink_prop_2.quark_propagator_id);

          write(xml_out, "forward_prop_corr_1", sumMulti(localNorm2(sink_prop_1), phases.getSet()));
          write(xml_out, "forward_prop_corr_2", sumMulti(localNorm2(sink_prop_2), phases.getSet()));
        }
        pop(xml_out);


        push(xml_out, "SourceSinkType");
        {
          QDPIO::cout << "Source_type_1 = " << all_sinks.sink_prop_1.source_type << std::endl;
          QDPIO::cout << "Sink_type_1 = " << all_sinks.sink_prop_1.sink_type << std::endl;
          QDPIO::cout << "Source_type_2 = " << all_sinks.sink_prop_2.source_type << std::endl;
          QDPIO::cout << "Sink_type_2 = " << all_sinks.sink_prop_2.sink_type << std::endl;

          write(xml_out, "source_type_1", all_sinks.sink_prop_1.source_type);
          write(xml_out, "source_disp_type_1", all_sinks.sink_prop_1.source_disp_type);
          write(xml_out, "sink_type_1", all_sinks.sink_prop_1.sink_type);
          write(xml_out, "sink_disp_type_1", all_sinks.sink_prop_1.sink_disp_type);

          write(xml_out, "source_type_2", all_sinks.sink_prop_2.source_type);
          write(xml_out, "source_disp_type_2", all_sinks.sink_prop_2.source_disp_type);
          write(xml_out, "sink_type_2", all_sinks.sink_prop_2.sink_type);
          write(xml_out, "sink_disp_type_2", all_sinks.sink_prop_2.sink_disp_type);
        }
        pop(xml_out);


        // References for use later
        const LatticePropagator& sink_prop_1 = 
          TheNamedObjMap::Instance().getData<LatticePropagator>(all_sinks.sink_prop_1.quark_propagator_id);
        const LatticePropagator& sink_prop_2 = 
          TheNamedObjMap::Instance().getData<LatticePropagator>(all_sinks.sink_prop_2.quark_propagator_id);


        // Construct group name for output
        std::string src_type;
        if (all_sinks.sink_prop_1.source_type == "POINT_SOURCE")
          src_type = "Point";
        else if (all_sinks.sink_prop_1.source_type == "SF_POINT_SOURCE")
          src_type = "Point";
        else if (all_sinks.sink_prop_1.source_type == "SHELL_SOURCE")
          src_type = "Shell";
        else if (all_sinks.sink_prop_1.source_type == "SHELL_SOURCE_FAST")
          src_type = "Shell";
        else if (all_sinks.sink_prop_1.source_type == "NORM_SHELL_SOURCE")
          src_type = "Shell";
        else if (all_sinks.sink_prop_1.source_type == "SF_SHELL_SOURCE")
          src_type = "Shell";
        else if (all_sinks.sink_prop_1.source_type == "WALL_SOURCE")
          src_type = "Wall";
        else if (all_sinks.sink_prop_1.source_type == "SF_WALL_SOURCE")
          src_type = "Wall";
        else if (all_sinks.sink_prop_1.source_type == "RAND_ZN_WALL_SOURCE")
          src_type = "Wall";
        else
        {
          QDPIO::cerr << "Unsupported source type = " << all_sinks.sink_prop_1.source_type << std::endl;
          QDP_abort(1);
        }

        std::string snk_type;
        if (all_sinks.sink_prop_1.sink_type == "POINT_SINK")
          snk_type = "Point";
        else if (all_sinks.sink_prop_1.sink_type == "SHELL_SINK")
          snk_type = "Shell";
        else if (all_sinks.sink_prop_1.sink_type == "SHELL_SINK_FAST")
          snk_type = "Shell";
        else if (all_sinks.sink_prop_1.sink_type == "NORM_SHELL_SINK")
          snk_type = "Shell";
        else if (all_sinks.sink_prop_1.sink_type == "WALL_SINK")
          snk_type = "Wall";
        else
        {
          QDPIO::cerr << "Unsupported sink type = " << all_sinks.sink_prop_1.sink_type << std::endl;
          QDP_abort(1);
        }

        std::string source_sink_type = src_type + "_" + snk_type;
        QDPIO::cout << "Source type = " << src_type << std::endl;
        QDPIO::cout << "Sink type = "   << snk_type << std::endl;


        //-----------------------------------------------------------
        // Calculate nucleon 2pt function with gamma projections 
        //-----------------------------------------------------------
        if(params.param.multi_srcsP)
          nucl_2pt_proj(sink_prop_2, sink_prop_1, phases, 
            t0, params.param.multi_srcs, bc_spec,
            params.param.axial_proj_only,
            xml_out, source_sink_type + "_Wilson_Baryons", params.param.cEDM);
        else
          nucl_2pt_proj(sink_prop_2, sink_prop_1, phases, 
            t0, bc_spec, params.param.axial_proj_only,
            xml_out, source_sink_type + "_Wilson_Baryons", params.param.cEDM);

        pop(xml_out);  // array element

        delete p_phases;
      }
      pop(xml_out);  // Wilson_nucl_2pt_proj_measurements 
      pop(xml_out);  // nucl_2pt_proj

      snoop.stop();
      QDPIO::cout << InlineNuclTwoptProjEnv::name << ": total time = "
        << snoop.getTimeInSeconds() 
        << " secs" << std::endl;

      QDPIO::cout << InlineNuclTwoptProjEnv::name << ": ran successfully" << std::endl;

      END_CODE();
    } 

};
