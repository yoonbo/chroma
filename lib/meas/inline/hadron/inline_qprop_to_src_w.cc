// $Id: inline_qprop_to_src_w.cc,v 3.2 2006-11-28 19:30:03 edwards Exp $
/*! \file
 * \brief Inline measurement of qprop_to_src
 *
 * Addition of props
 */

#include "meas/inline/hadron/inline_qprop_to_src_w.h"
#include "meas/inline/abs_inline_measurement_factory.h"

#include "meas/inline/io/named_objmap.h"

namespace Chroma 
{ 
  //! Propagator parameters
  void read(XMLReader& xml, const std::string& path, InlineQpropToSrcEnv::Params::NamedObject_t& input)
  {
    XMLReader inputtop(xml, path);

    read(inputtop, "prop",   input.prop);
    read(inputtop, "source", input.source);
    read(inputtop, "t_ins", input.t_ins);
    read(inputtop, "gamma_ins", input.gamma_ins);
  }

  //! Propagator parameters
  void write(XMLWriter& xml, const std::string& path, const InlineQpropToSrcEnv::Params::NamedObject_t& input)
  {
    push(xml, path);

    write(xml, "prop", input.prop);
    write(xml, "source", input.source);
    write(xml, "t_ins", input.t_ins);
    write(xml, "gamma_ins", input.gamma_ins);
    
    pop(xml);
  }


  namespace InlineQpropToSrcEnv 
  { 
    namespace
    {
      AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
          const std::string& path) 
      {
        return new InlineMeas(Params(xml_in, path));
      }

      //! Local registration flag
      bool registered = false;
    }

    const std::string name = "QPROP_TO_SRC";

    // Anonymous namespace
    namespace TSF
    {
      // Standard Time Slicery
      class TimeSliceFunc : public SetFunc
      {
      public:
        TimeSliceFunc(int dir): dir_decay(dir) {}
        int operator() (const multi1d<int>& coordinate) const {return coordinate[dir_decay];}
        int numSubsets() const {return Layout::lattSize()[dir_decay];}
        int dir_decay;
      private:
        TimeSliceFunc() {}  // hide default constructor
      };
    }

    //! Register all the factories
    bool registerAll() 
    {
      bool success = true; 
      if (! registered)
      {
        success &= TheInlineMeasurementFactory::Instance().registerObject(name, createMeasurement);
        registered = true;
      }
      return success;
    }

    // Param stuff
    Params::Params() { frequency = 0; }

    Params::Params(XMLReader& xml_in, const std::string& path) 
    {
      try 
      {
        XMLReader paramtop(xml_in, path);

        if (paramtop.count("Frequency") == 1)
          read(paramtop, "Frequency", frequency);
        else
          frequency = 1;

        // Parameters for source construction
        // Read in the output propagator/source configuration info
        read(paramtop, "NamedObject", named_obj);
      }
      catch(const std::string& e) 
      {
        QDPIO::cerr << __func__ << ": Caught Exception reading XML: " << e << std::endl;
        QDP_abort(1);
      }
    }


    void
      Params::writeXML(XMLWriter& xml_out, const std::string& path) 
      {
        push(xml_out, path);

        // Write out the output propagator/source configuration info
        write(xml_out, "NamedObject", named_obj);

        pop(xml_out);
      }

    //--------------------------------------------------------------


    // Function call
    void 
      InlineMeas::operator()(unsigned long update_no,
          XMLWriter& xml_out) 
      {
        START_CODE();

        push(xml_out, "qprop_to_src");
        write(xml_out, "update_no", update_no);

        QDPIO::cout << "QPROP_TO_SRC: propagator transformation utility" << std::endl;

        // Write out the input
        params.writeXML(xml_out, "Input");

        //
        // Read in the source along with relevant information.
        // 
        XMLReader propA_file_xml, propA_record_xml;

        LatticePropagator prop ;
        LatticePropagator source ;
        QDPIO::cout << "Snarf the props from a named buffer" << std::endl;
        try
        {
          prop   = TheNamedObjMap::Instance().getData<LatticePropagator>(params.named_obj.prop);
          source = TheNamedObjMap::Instance().getData<LatticePropagator>(params.named_obj.source);
        }    
        catch (std::bad_cast)
        {
          QDPIO::cerr << name << ": caught dynamic cast error" 
            << std::endl;
          QDP_abort(1);
        }
        catch (const std::string& e) 
        {
          QDPIO::cerr << name << ": error extracting source_header: " << e << std::endl;
          QDP_abort(1);
        }


        // Machinery to do timeslice replication with
        Set TS;
        TS.make(TSF::TimeSliceFunc(3));

        source = zero;

        if(params.named_obj.t_ins == -1)
          source = Gamma(params.named_obj.gamma_ins)*prop;
        else if(params.named_obj.t_ins == -2)
          source = Gamma(params.named_obj.gamma_ins)*adj(prop);
        else
          source[TS[params.named_obj.t_ins]] = Gamma(params.named_obj.gamma_ins)*prop;


        /*
         *  Write the a source out to a named buffer
         */
        try
        {
          QDPIO::cout << "Attempt to store sequential source" << std::endl;

          // Store the seqsource
          TheNamedObjMap::Instance().getData<LatticePropagator>(params.named_obj.source) = source ;

          QDPIO::cout << "Propagator successfully stored"  << std::endl;
        }
        catch (std::bad_cast)
        {
          QDPIO::cerr << name << ": dynamic cast error" 
            << std::endl;
          QDP_abort(1);
        }
        catch (const std::string& e) 
        {
          QDPIO::cerr << name << ": error storing seqsource: " << e << std::endl;
          QDP_abort(1);
        }


        pop(xml_out);   // qprop_to_src

        QDPIO::cout << "QpropToSrc ran successfully" << std::endl;

        END_CODE();
      }

  }

}  
