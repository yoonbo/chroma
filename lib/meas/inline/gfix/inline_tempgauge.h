/*! \file
 *  \brief Inline temporal gauge fixing loops
 */

#ifndef __inline_temporal_gfix_h__
#define __inline_temporal_gfix_h__

#include "chromabase.h"
#include "meas/inline/abs_inline_measurement.h"

namespace Chroma 
{ 
  /*! \ingroup inlinegfix */
  namespace InlineTempGaugeEnv 
  {
    extern const std::string name;
    bool registerAll();

    //! Parameter structure
    /*! \ingroup inlinegfix */
    struct Params 
    {
      Params();
      Params(XMLReader& xml_in, const std::string& path);
      void write(XMLWriter& xml_out, const std::string& path);

      unsigned long frequency;

      struct Param_t
      {
	int  j_decay;     /*!< direction perpendicular to slices to be gauge fixed */
      } param;

      struct NamedObject_t
      {
	std::string     gauge_id;      /*!< input gauge field */
	std::string     gfix_id;       /*!< output gauge field */
      } named_obj;

    };


    //! Inline measurement of Temporal gauge fixing
    /*! \ingroup inlinegfix */
    class InlineMeas : public AbsInlineMeasurement 
    {
    public:
      ~InlineMeas() {}
      InlineMeas(const Params& p) : params(p) {}
      InlineMeas(const InlineMeas& p) : params(p.params) {}

      unsigned long getFrequency(void) const {return params.frequency;}

      //! Do the measurement
      void operator()(const unsigned long update_no,
		      XMLWriter& xml_out); 

    private:
      Params params;
    };

  }

}

#endif
